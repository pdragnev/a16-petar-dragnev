package DSA.recursion;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Fibonacci {
    private static Map<Integer,Long> memo = new HashMap<>();
    static {
        memo.put(0,0L); //fibonacci(0)
        memo.put(1,1L); //fibonacci(1)
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(fibonacci(Integer.parseInt(scanner.nextLine())));
    }

    public static long fibonacci(int x) {
        return memo.computeIfAbsent(x, n -> Math.addExact(fibonacci(n-1),
                fibonacci(n-2)));
    }
}
