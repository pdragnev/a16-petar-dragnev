package DSA.recursion;

import java.util.Scanner;

public class CountHi {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(countHi(scanner.nextLine()));

    }

    public static int countHi(String str) {
        if (str.length() == 0) {
            return 0;
        }
        return countHi(str.substring(1)) + (str.length() > 1 && str.substring(0, 2).equals("hi") ? 1: 0);
    }
}
