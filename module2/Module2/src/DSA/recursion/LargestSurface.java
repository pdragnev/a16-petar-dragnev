package DSA.recursion;

import java.util.Scanner;

public class LargestSurface {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] rowsCols = scanner.nextLine().split("\\s");
        String[][] matrix = new String[Integer.parseInt(rowsCols[0])][Integer.parseInt(rowsCols[1])];
        int[][] matrixValues = new int[Integer.parseInt(rowsCols[0])][Integer.parseInt(rowsCols[1])];
        for (int i = 0; i < matrix.length; i++) {
            matrix[i] = scanner.nextLine().split("\\s");
        }
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrixValues[i][j] = Integer.parseInt(matrix[i][j]);
            }
        }


        System.out.println(largestSurface(matrixValues));

    }

    public static int largestSurface(int[][] grid) {
        int largestSurface = 0;
        if (grid.length == 0) {
            return largestSurface;
        }
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (!(grid[i][j] == -1)) {
                    int numberToSearch = grid[i][j];
                    int currentSurface = findPath(grid, i, j, numberToSearch);
                    if (currentSurface > largestSurface) {
                        largestSurface = currentSurface;
                    }
                }
            }
        }
        return largestSurface;
    }

    private static int findPath(int[][] grid, int row, int col, int numberToSearch) {
        if (outOfGrid(grid, row, col)) {
            return 0;
        }
        if (!(grid[row][col] == numberToSearch)) {
            return 0;
        }
        grid[row][col] = -1;

        return findPath(grid, row + 1, col, numberToSearch) +
                findPath(grid, row - 1, col, numberToSearch) +
                findPath(grid, row, col + 1, numberToSearch) +
                findPath(grid, row, col - 1, numberToSearch) + 1;
    }

    private static boolean outOfGrid(int[][] grid, int row, int col) {
        return row < 0 || col < 0 || row >= grid.length || col >= grid[row].length;
    }
}
