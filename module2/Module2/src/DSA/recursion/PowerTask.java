package DSA.recursion;

public class PowerTask {
    public static void main(String[] args) {
        System.out.println(power(5, 5));
    }

    private static int power(int base, int exponent) {
        if (exponent <= 0) {
            return 1;
        }
        return
                power(base, exponent - 1) * base;
    }
}
