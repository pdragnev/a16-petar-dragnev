package DSA.recursion;

import java.util.Scanner;

public class CountX {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(countX(scanner.nextLine()));

    }

    public static int countX(String number) {
        if (number.length() == 0) {
            return 0;
        }
        return countX(number.substring(1)) + (number.charAt(0) == 'x' ? 1 : 0);
    }
}

