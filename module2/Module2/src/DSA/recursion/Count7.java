package DSA.recursion;

import java.util.Scanner;

public class Count7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(count7(Integer.parseInt(scanner.nextLine())));

    }

    public static int count7(int number) {
        if (number <= 0) {
            return 0;
        }
        return count7(number / 10) + (number % 10 == 7 ? 1 : 0);
    }
}
