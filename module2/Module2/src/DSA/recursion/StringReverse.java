package DSA.recursion;

public class StringReverse {
    public static void reverse(String text, int index) {
        if (index >= text.length()) {
            return;
        }
        reverse(text, index + 1);
        System.out.print(text.charAt(index));

    }

    public static String reverse(String text) {
        if (text.length() <= 1) {
            return text;
        }

        return reverse(text.substring(1)) + text.charAt(0);
    }
}
