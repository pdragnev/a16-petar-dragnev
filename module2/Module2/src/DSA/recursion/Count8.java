package DSA.recursion;

import java.util.Scanner;

public class Count8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(count8(Integer.parseInt(scanner.nextLine())));

    }

    public static int count8(int number) {
        if (number <= 0) {
            return 0;
        }
        return count8(number / 10) + (number % 10 == 8 ? ((number / 10) % 10 == 8 ? 2 : 1) : 0);
    }
}
