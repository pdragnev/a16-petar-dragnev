package DSA.recursion;

import java.util.Scanner;

public class SumDigits {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(sumDigits(Integer.parseInt(scanner.nextLine())));

    }

    public static int sumDigits (int number){
        if (number <=0){
            return 0;
        }

        return sumDigits(number/10) + number%10;
    }
}
