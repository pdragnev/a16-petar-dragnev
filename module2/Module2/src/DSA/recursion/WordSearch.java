package DSA.recursion;

public class WordSearch {
    public static void main(String[] args) {
        char[][] board = {
                {'A', 'B', 'C', 'E'},
                {'S', 'F', 'C', 'S'},
                {'A', 'D', 'E', 'E'}
        };
        String word = "ABCCED";
        System.out.println(exist(board, word));
    }

    public static   boolean exist(char[][] board, String word) {
        if (board.length == 0) {
            return false;
        }
        boolean[][] visited = new boolean[board.length][board[0].length];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if (board[i][j] == word.charAt(0) && dfs(board, word, 0, i, j, visited))

                    return true;


            }
        }
        return false;
    }

    private static boolean dfs(char[][] board, String word, int index, int row, int col, boolean[][] visited) {
        if (index == word.length()) {
            return true;
        }
        if (outOfBoard(board, row, col) || visited[row][col]) {
            return false;
        }
        if (word.charAt(index) != board[row][col]) {
            return false;
        }
        visited[row][col] = true;
        boolean result =
                dfs(board, word, index + 1, row + 1, col, visited)||
                        dfs(board, word, index + 1, row - 1, col, visited)||
                        dfs(board, word, index + 1, row, col + 1, visited)||
                        dfs(board, word, index + 1, row, col - 1, visited);
        visited[row][col] = false;
        return result;
    }

    private static boolean outOfBoard(char[][] board, int row, int col) {
        return row < 0 || col < 0 || row >= board.length || col >= board[row].length;
    }
}

