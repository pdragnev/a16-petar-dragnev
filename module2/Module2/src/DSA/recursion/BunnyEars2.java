package DSA.recursion;

import java.util.Scanner;

public class BunnyEars2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(ears(Integer.parseInt(scanner.nextLine())));
    }

    public static int ears(int bunnies) {
        if (bunnies <= 0) {
            return 0;
        }
        if (bunnies % 2 == 0) {
            return ears(bunnies - 1) + 3;
        } else {
            return ears(bunnies - 1) + 2;
        }

    }
}
