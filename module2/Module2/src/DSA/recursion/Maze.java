package DSA.recursion;

public class Maze {
    private static final char WALLY = '\u25A0';
    private static final char EMPTY = '\u25CB';
    private static final char VISITED = '\u25CD';
    private static final char EXIT = '\u25A1';

    public static void main(String[] args) {
        char[][] coolMaze = {
                {EMPTY, WALLY, EMPTY, EMPTY, WALLY, EXIT},
                {EMPTY, WALLY, WALLY, EMPTY, WALLY, EMPTY},
                {EMPTY, EMPTY, EMPTY, WALLY, WALLY, EMPTY},
                {EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY},
                {EMPTY, WALLY, WALLY, EMPTY, WALLY, EMPTY},
        };
        //printMaze(coolMaze);
        findPath(coolMaze, 0, 0);
    }

    private static void findPath(char[][] maze, int row, int col) {
        if (outOfMaze(maze, row, col)) {
            return;
        }
        if (maze[row][col] == EXIT) {
            System.out.printf("Exit is found at (%d, %d)%n", row, col);
            printMaze(maze);
            return;
        }
        if (maze[row][col] == WALLY) {
            return;
        }
        if (maze[row][col] == VISITED) {
            return;
        }


        maze[row][col] = VISITED;

        // printMaze(maze);

        findPath(maze, row + 1, col);
        findPath(maze, row - 1, col);
        findPath(maze, row, col + 1);
        findPath(maze, row, col - 1);

        // maze[row][col] = EMPTY;
    }

    private static void printMaze(char[][] maze) {
        for (int i = 0; i < maze.length; i++) {
            System.out.print('|');
            for (int j = 0; j < maze[i].length; j++) {
                System.out.print(maze[i][j]);
                System.out.print('|');
            }
            System.out.println();
        }
        System.out.println();
    }

    private static boolean outOfMaze(char[][] maze, int row, int col) {
        return row < 0 || col < 0 || row >= maze.length || col >= maze[row].length;
    }
}

