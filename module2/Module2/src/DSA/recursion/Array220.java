package DSA.recursion;

import java.util.Scanner;

public class Array220 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String [] input = scanner.nextLine().split(",");
        int index = Integer.parseInt(scanner.nextLine());
        System.out.println(containsx10(input, index));
    }

    public static boolean containsx10(String [] input, int startIndex){
        if (startIndex + 1 >= input.length){
            return false;
        }
        if (input[startIndex + 1].equals(String.valueOf(Integer.parseInt(input[startIndex]) * 10))){
            return true;
        }
        return containsx10(input,startIndex+1);
    }
}