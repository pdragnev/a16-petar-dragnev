package DSA.recursion;

import java.util.Scanner;

public class Triangle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(triagle(Integer.parseInt(scanner.nextLine())));
    }

    public static int triagle (int rows){
        if (rows<=0){
            return 0;
        }
        return rows + triagle(rows-1);
    }
}
