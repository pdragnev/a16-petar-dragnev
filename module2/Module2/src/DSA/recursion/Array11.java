package DSA.recursion;

import java.util.Scanner;

public class Array11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String [] input = scanner.nextLine().split(",");
        int index = Integer.parseInt(scanner.nextLine());
        System.out.println(contains(input, index, "11"));
    }

    public static int contains(String [] input, int startIndex, String symbolToContain){
        if (startIndex >= input.length){
            return 0;
        }

        return contains(input,startIndex+1,symbolToContain) +
                (input[startIndex].equals(symbolToContain) ? 1 : 0);
    }
}

