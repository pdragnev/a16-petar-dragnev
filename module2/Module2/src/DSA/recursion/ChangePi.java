package DSA.recursion;

import java.util.Scanner;

public class ChangePi {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(changePi(scanner.nextLine()));
    }

    public static String changePi(String str){
        if (str.length() < 1) {
            return str;
        }
        else {
            int sub = 1;
            String first = String.valueOf(str.charAt(0));
            if (str.length()>1 && first.equals("p")) {
                String text = str.substring(0,2);
                 first = "pi".equals(text) ? "3.14" : text;
                 sub = 2;
            }
            return first + changePi(str.substring(sub));
        }
    }
}
