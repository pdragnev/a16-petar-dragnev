package DSA.recursion;

import java.util.Scanner;

public class Array6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String [] input = scanner.nextLine().split(",");
        int index = Integer.parseInt(scanner.nextLine());
        System.out.println(contains6(input, index));
    }

    public static boolean contains6 (String [] input, int startIndex){
        if (startIndex >= input.length){
            return false;
        }
        if (input[startIndex].equals("6")){
            return true;
        }
       return contains6(input,startIndex+1);
    }
}
