package DSA.recursion;

import java.util.Arrays;

public class NumberIslands {

    public static void main(String[] args) {
        char[][] ocean = {
                {'1', '1', '1', '1', '0'},
                {'1', '1', '0', '1', '0'},
                {'1', '1', '0', '0', '1'},
                {'0', '0', '0', '1', '1'}
        };

        System.out.println(numIslands(ocean));
        System.out.println(Arrays.deepToString(ocean));

    }

    public static int numIslands(char[][] grid) {
        int count = 0;
        if (grid.length == 0) {
            return count;
        }
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == '1') {
                    findPath(grid, i, j);
                    count++;
                }
            }
        }
        return count;
    }

    private static void findPath(char[][] maze, int row, int col) {
        if (outOfMaze(maze, row, col)) {
            return;
        }
        if (maze[row][col] == '0' || maze[row][col] == 'x') {
            return;
        }
        maze[row][col] = 'x';

        findPath(maze, row + 1, col);
        findPath(maze, row - 1, col);
        findPath(maze, row, col + 1);
        findPath(maze, row, col - 1);
    }

    private static boolean outOfMaze(char[][] maze, int row, int col) {
        return row < 0 || col < 0 || row >= maze.length || col >= maze[row].length;
    }
}
