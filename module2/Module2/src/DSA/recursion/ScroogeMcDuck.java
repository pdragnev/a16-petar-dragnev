package DSA.recursion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ScroogeMcDuck {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String[] inp = br.readLine().split(" ");
        int n = Integer.parseInt(inp[0]);
        int m = Integer.parseInt(inp[1]);
        int[][] matrix = new int[n][m];
        int[] start = new int[2];

        for (int i = 0; i < n; i++) {
            String[] next = br.readLine().split(" ");
            for (int j = 0; j < m; j++) {
                matrix[i][j] = Integer.parseInt(next[j]);
                if (matrix[i][j] == 0) {
                    start[0] = i;
                    start[1] = j;
                }
            }
        }
        goldCount(matrix, start[0], start[1]);
        System.out.println(gold);
    }

    private static int gold = 0;

    private static void goldCount(int[][] matrix, int row, int col) {

        int[] nextCell = nextCellCoordinates(matrix, row, col);
        if (nextCell[0] == -1 && nextCell[1] == -1) {
            return;
        }
        gold++;
        matrix[nextCell[0]][nextCell[1]]--;
        goldCount(matrix, nextCell[0], nextCell[1]);
    }

    private static int[] nextCellCoordinates(int[][] matrix, int row, int col) {

        int dir=0;
        int maxCoins = 0;
        int next_row,next_col;
        int [] xMoves = {0,0,-1,1};
        int [] yMoves = {-1,1,0,0};
        for (int i = 0; i < xMoves.length ; i++) {
            next_row = row + xMoves[i];
            next_col = col + yMoves[i];
            if (isSafe(next_row,next_col,matrix,maxCoins)){
                maxCoins=matrix[next_row][next_col];
                dir=i+1;
            }
        }
        switch (dir){
            case 1: return new int[]{row, col + -1};
            case 2: return new int[]{row,col +1};
            case 3: return new int[]{row +-1,col};
            case 4: return new int[]{row +1,col};
        }

        return new int[]{-1,-1};
    }

    static boolean isSafe(int x, int y, int[][] sol, int maxCoins) {
        return (x >= 0 && x < sol.length && y >= 0 &&
                y < sol[0].length && sol[x][y]>maxCoins);
    }
}