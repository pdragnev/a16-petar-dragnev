package DSA.recursion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.TreeSet;

public class Variations {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int number = Integer.parseInt(br.readLine());

        String[] inp = br.readLine().split(" ");

        brute(inp, number, new StringBuffer());
        tSet.forEach(System.out::println);
    }
    static TreeSet tSet = new TreeSet();

    private static void brute(String[] input, int depth, StringBuffer output) {
        if (depth == 0) {
            tSet.add(output.toString());
            return;
        }

        for (String s : input) {
            output.append(s);
            brute(input, depth - 1, output);
            output.delete(output.length() - s.length(), output.length());
        }

    }
}

/*import java.util.Scanner;

public class Variations {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        char[] input = scanner.nextLine().replace(" ", "").toCharArray();
        printAllKLength(input,n);
    }

    static void printAllKLength(char[] set, int k) {
        int n = set.length;
        printAllKLengthRec(set, "", n, k);
    }

    static void printAllKLengthRec(char[] set, String prefix, int n, int k) {
        if (k == 0) {
            System.out.println(prefix);
            return;
        }
        for (int i = 0; i < n; ++i) {
            String newPrefix = prefix + set[i];
            printAllKLengthRec(set, newPrefix, n, k - 1);
        }
    }

}*/
