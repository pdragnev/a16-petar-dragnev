package DSA.recursion;

import java.util.Scanner;

public class PowerN {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int base = Integer.parseInt(scanner.nextLine());
        int exponent = Integer.parseInt(scanner.nextLine());
        System.out.println(power(base,exponent));
    }
    public static int power (int base, int exponent){
        if (exponent<=0){
            return 1;
        }
        return power(base, exponent - 1) * base;
    }
}
