package DSA.finalpractise;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

public class SantaHelper {
    static class Item implements Comparable<Item> {
        String name;
        double price;
        String kidName;

        Item(String name, double age, String kidName) {
            this.name = name;
            this.price = age;
            this.kidName = kidName;
        }

        @Override
        public String toString() {
            return String.format("{%s;%s;%.2f}", this.name, this.kidName, this.price);
        }

        @Override
        public int compareTo(Item o) {
            return this.kidName.compareTo(o.kidName);
        }
    }
    private static HashMap<String, List<Item>> kids = new HashMap<>();

    public static void main(String[] args) throws IOException {
        BufferedReader reader
                = new BufferedReader(new InputStreamReader(System.in));
        int numberofCommands = Integer.parseInt(reader.readLine());
        while (numberofCommands > 0) {
            String input = reader.readLine();
            String[] command = input.split(" ");
            String[] parameters = parseParameters(input);
            switch (command[0]) {
                case "AddWish":
                    System.out.println(addWish(parameters));
                    break;
                case "DeleteWishes":
                    System.out.println(deleteWish(parameters));
                    break;
                case "FindWishesByPriceRange":
                    findWishesByPrice(parameters);
                    break;
                case "FindWishesByChild":
                    findWishes(parameters);
                    break;
            }
            numberofCommands--;
        }
    }


    private static String[] parseParameters(String fullCommand) {
        String[] commandParametersSplit = fullCommand.split(" ", 2);
        if (commandParametersSplit.length == 1 || commandParametersSplit[1].equals("")) {
            return new String[0];
        }
        return commandParametersSplit[1].split(";");
    }

    private static String addWish(String[] command) {
        String kidName = command[2];
        Item item = new Item(command[0], Double.parseDouble(command[1]), kidName);

        kids.put(kidName, kids.getOrDefault(kidName, new ArrayList<>()));
        kids.get(kidName).add(item);

        return "Wish added";
    }

    private static String deleteWish(String[] command) {
        String kidName = command[0];

        if (!kids.containsKey(kidName)){
            return "No Wishes found";
        }
        int wishes = kids.get(kidName).size();
        kids.remove(kidName);

        return String.format("%d Wishes deleted", wishes);
    }

    private static void findWishes(String[] command) {
        String kidName = command[0];

        List<Item> wishes = new ArrayList<>();
        if (kids.containsKey(kidName)) {
            wishes = kids.get(kidName);
        } else {
            System.out.println("No Wishes found");
        }
        wishes.sort(Comparator.comparing(item -> item.name));
        for (Item item : wishes) {
            System.out.printf("%s%n", item.toString());
        }
    }

    private static void findWishesByPrice(String[] command) {
        String kidName = command[0];

        List<Item> filteredWishes = kids.values()
                .stream()
                .flatMap(List::stream)
                .collect(Collectors.toList());

        List<Item> result = new ArrayList<>();

        for (Item item:filteredWishes) {
            if (item.price>=Double.parseDouble(command[0]) && item.price <= Double.parseDouble(command[1]) ){
                result.add(item);
            }
        }

        if (result.size() == 0) {
            System.out.println("No Wishes found");
            return;
        }
        result.sort(Comparator.comparing(p -> p.name));
        for (Item item : result) {
            System.out.printf("%s%n", item.toString());
        }
    }
}