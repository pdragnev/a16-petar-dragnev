package DSA.finalpractise;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ArmyLunch {
    public static void main(String[] args) throws IOException {
        BufferedReader reader
                = new BufferedReader(new InputStreamReader(System.in));
        int numberSoldiers = Integer.parseInt(reader.readLine());
        String[] input = reader.readLine().split(" ");
        ArrayList<String> sergants = new ArrayList<>();
        ArrayList<String> corporal = new ArrayList<>();
        ArrayList<String> privateSoldiers = new ArrayList<>();

        for (String s : input) {
            if (s.charAt(0) == 'C') {
                corporal.add(s);
            } else if (s.charAt(0) == 'S') {
                sergants.add(s);
            } else {
                privateSoldiers.add(s);
            }
        }
        ArrayList<String> result = new ArrayList<>();
        result.addAll(sergants);
        result.addAll(corporal);
        result.addAll(privateSoldiers);

        System.out.println(result.toString().replace("[", "").replace("]", "").replace(",", ""));
    }
}
