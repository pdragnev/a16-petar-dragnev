package DSA.finalpractise;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class SmallWorld {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] rowsCols = scanner.nextLine().split(" ");
        String[][] matrix = new String[Integer.parseInt(rowsCols[0])][Integer.parseInt(rowsCols[1])];
        int[][] matrixValues = new int[Integer.parseInt(rowsCols[0])][Integer.parseInt(rowsCols[1])];
        for (int i = 0; i < matrix.length; i++) {
            matrix[i] = scanner.nextLine().split("");
        }
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrixValues[i][j] = Integer.parseInt(matrix[i][j]);
            }
        }


        List<Integer> result = smallWorlds(matrixValues);
        Collections.sort(result);
        Collections.reverse(result);
        for (Integer i:result) {
            System.out.println(i);
        }

    }

    public static ArrayList<Integer> smallWorlds(int[][] grid) {
        ArrayList<Integer> worlds = new ArrayList<>();
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == 0 || grid[i][j] == -1) {
                    continue;
                }

                worlds.add(findPath(grid, i, j));


            }
        }
        return worlds;
    }

    private static int findPath(int[][] maze, int row, int col) {
        if (outOfMaze(maze, row, col)) {
            return 0;
        }
        if (maze[row][col] == 0 || maze[row][col] == -1) {
            return 0;
        }
        maze[row][col] = -1;

        return findPath(maze, row + 1, col) +
                findPath(maze, row - 1, col) +
                findPath(maze, row, col + 1) +
                findPath(maze, row, col - 1) + 1;
    }

    private static boolean outOfMaze(int[][] maze, int row, int col) {
        return row < 0 || col < 0 || row >= maze.length || col >= maze[row].length;
    }
}