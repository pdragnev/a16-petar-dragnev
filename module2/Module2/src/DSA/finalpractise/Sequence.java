package DSA.finalpractise;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

public class Sequence {
    public static void main(String[] args) throws IOException {
        BufferedReader reader
                = new BufferedReader(new InputStreamReader(System.in));

        String[] input = reader.readLine().split(" ");
        ArrayList<Integer> sequence = new ArrayList<>();
        int k = Integer.parseInt(input[0]);
        int n = Integer.parseInt(input[1]);

        sequence.add(k);
        int j = 0;
        int currentN = sequence.get(j++);

        for (int i = 0; i < n; i++) {
            if (i%3 == 0){
                sequence.add(currentN+1);
            } else if (i%3 == 1){
                sequence.add(2*currentN+1);
            } else {
                sequence.add(currentN+2);
                currentN = sequence.get(j++);
            }
        }

        System.out.println(sequence.get(sequence.size()-2));
    }
}
