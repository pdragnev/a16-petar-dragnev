package DSA.citymap;

import java.util.*;

public class MainCityTask {
    public static void main(String[] args) {
        Map<City, List<Person>>  myCity = new TreeMap<>();
        List<Person> people = new ArrayList<>();
        people.add(new Person("petar","dragnev"));
        people.add(new Person("alex","petrov"));
        people.add(new Person("nadya","sau"));
        myCity.put(new City("Sofia","Bulgaria","1700"),people);
        myCity.put(new City("Plovdiv","Bulgaria","4004"),people);

        System.out.println(myCity);
    }
}
