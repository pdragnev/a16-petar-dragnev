package DSA.citymap;

import java.util.Objects;

public class City implements Comparable<City>{
    private String name;
    private String countryName;
    private String postalCode;

    public City(String name, String countryName, String postalCode) {
        this.name = name;
        this.countryName = countryName;
        this.postalCode = postalCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return Objects.equals(name, city.name) &&
                Objects.equals(countryName, city.countryName) &&
                Objects.equals(postalCode, city.postalCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, countryName, postalCode);
    }


    @Override
    public String toString() {
        return "City-" +
                "name=" + name +
                ", countryName=" + countryName  +
                ", postalCode=" + postalCode ;
    }

    @Override
    public int compareTo(City o) {
        if (this.name.compareTo(o.name) == 0){
            return this.countryName.compareTo(o.countryName);
        }

        return this.name.compareTo(o.name);
    }
}
