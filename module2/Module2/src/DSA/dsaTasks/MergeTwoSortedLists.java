package DSA.dsaTasks;

public class MergeTwoSortedLists {
    public static ListNode mergeTwoLists(ListNode list1, ListNode list2) {


        ListNode current = new ListNode(0);
        ListNode temp = current;

        while (true) {

            if (list1 == null) {
                temp.next = list2;
                break;
            }

            if (list2 == null) {
                temp.next = list1;
                break;
            }

            if (list1.val < list2.val) {
                temp.next = list1;
                temp = temp.next;
                list1 = list1.next;
            } else {
                temp.next = list2;
                temp = temp.next;
                list2 = list2.next;
            }


        }

        return current.next;
    }
}
     /*   ArrayList<ListNode> listNodes = new ArrayList<>();

        while (list1!=null){
            listNodes.add(list1);
            list1 = list1.next;
        }
        while (list2!=null){
            listNodes.add(list2);
            list2 = list2.next;
        }
        listNodes = (ArrayList<ListNode>) listNodes.stream().sorted(Comparator.comparing(node -> node.val)).collect(Collectors.toList());

        if (listNodes.isEmpty()){
            return null;
        }
        ListNode head = new ListNode(listNodes.get(0).val);
        ListNode temp = head;
        for (int i = 1; i < listNodes.size(); i++) {
            temp.next = new ListNode(listNodes.get(i).val);
            temp = temp.next;
        }

        return head;
        }*/

