package DSA.dsaTasks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;

public class HDNLToy {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int linesInput = Integer.parseInt(scanner.nextLine());
        String[] input = new String[linesInput];
        for (int i = 0; i < linesInput; i++) {
            input[i] = scanner.nextLine();
        }

        Stack<String> stack = new Stack<>();
        Stack<String> stackHelper = new Stack<>();
        int temp = 0;
        for (String s : input) {
            if (stack.isEmpty()) {
                stack.push(s);
                stackHelper.push(s);
                temp = getIntVal(s);
                continue;
            }
            if (temp >= getIntVal(s)) {
                while (!stackHelper.isEmpty()) {
                    stack.push(stackHelper.pop());
                }
                temp = getIntVal(s);
                stack.push(s);
                stackHelper.push(s);
                continue;
            }
            if (getIntVal(stack.peek()) < getIntVal(s)) {
                stack.push(s);
                stackHelper.push(s);
            } else if (getIntVal(stack.peek()) == getIntVal(s)) {
                stack.push(stackHelper.pop());
                stack.push(s);
                stackHelper.push(s);
            } else if (getIntVal(stack.peek()) > getIntVal(s)) {
                while (getIntVal(stackHelper.peek()) >= getIntVal(s)) {
                    stack.push(stackHelper.pop());
                }
                stack.push(s);
                stackHelper.push(s);
            }

        }
        while (!stackHelper.isEmpty()) {
            stack.push(stackHelper.pop());
        }

        ArrayList<String> result = new ArrayList<>(Arrays.asList(stack.toString().replace("[", "")
                .replace("]", "").replace(",", "").split(" ")));

        for (int i = 0; i < result.size(); i++) {
            String curr = result.get(i);
            String newLine = getWhiteSpace(((getIntVal(curr)) - 1)) + "<" + result.get(i) + ">";
            result.set(i, newLine);
        }

        for (int i = 0; i < result.size(); i++) {
            if (!stackHelper.isEmpty() && stackHelper.peek().equals(result.get(i))) {
                result.set(i, result.get(i).replace("<", "</"));
                stackHelper.pop();
            } else {
                stackHelper.push(result.get(i));
            }
        }

        for (String s : result) {
            System.out.println(s);
        }
    }

    private static int getIntVal(String s) {
        return Integer.parseInt(s.substring(1));
    }

    private static String getWhiteSpace(int size) {
        if (size < 0) {
            size = 0;
        }
        StringBuilder builder = new StringBuilder(size);
        for (int i = 0; i < size; i++) {
            builder.append(' ');
        }
        return builder.toString();
    }

}
