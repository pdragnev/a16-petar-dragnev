package DSA.dsaTasks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Jumps {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int numberElements = Integer.parseInt(reader.readLine());
        int[] numbers = new int[numberElements];
        String[] input = reader.readLine().split(" ");
        for (int i = 0; i < numberElements; i++) {
            numbers[i] = Integer.parseInt(input[i]);
        }

        int counter;
        int maxCounter = 0;
        int temp;
        for (int i = 0; i < numberElements; i++) {
            counter = 0;
            temp = numbers[i];
            for (int j = i; j < numberElements; j++) {
                if (numbers[j] > temp) {
                    temp = numbers[j];
                    counter++;
                }
            }
            if (counter > maxCounter) {
                maxCounter = counter;
            }
            numbers[i] = counter;
        }

        System.out.println(maxCounter);
        System.out.println(Arrays.toString(numbers).replace("[", "")
                .replace("]", "").replace(",", ""));
    }
}