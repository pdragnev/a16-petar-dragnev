package DSA.dsaTasks;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class UniqueNumberOccurence {
    public boolean uniqueOccurrences(int[] arr) {

        Map<Integer, Integer> myMap = new HashMap<>();
        Set<Integer> mySet = new HashSet<>();

        for (Integer i : arr) {
            myMap.put(i, myMap.getOrDefault(i, 0) + 1);
        }

        for (Integer i : myMap.values()) {
            if (!mySet.add(i)) {
                return false;
            }
        }

        return true;
    }
}
