package DSA.dsaTasks;

import java.util.HashMap;
import java.util.Map;

public class FindDifference {
    public char findTheDifference(String s, String t) {
        if (s.isEmpty()) {
            return t.toCharArray()[0];
        }
        Map<Character, Integer> sMap = new HashMap<>();
        Map<Character, Integer> tMap = new HashMap<>();

        for (Character c : s.toCharArray()) {
            sMap.put(c, sMap.getOrDefault(c, 0) + 1);
        }

        for (Character c : t.toCharArray()) {
            tMap.put(c, tMap.getOrDefault(c, 0) + 1);
        }

        for (Character c : tMap.keySet()) {
            if (!tMap.get(c).equals(sMap.get(c))) {
                return c;
            }

        }

        return ' ';
    }
}
