package DSA.dsaTasks;

import java.util.*;

public class UncommonFromSentences {
    public String[] uncommonFromSentences(String A, String B) {

        Map<String, Integer> AMap = new HashMap<>();
        Map<String, Integer> BMap = new HashMap<>();

        for (String str : A.split(" ")) {
            AMap.put(str, AMap.getOrDefault(str, 0) + 1);
        }

        for (String str : B.split(" ")) {
            BMap.put(str, BMap.getOrDefault(str, 0) + 1);
        }

        Set<String> setKeys = new HashSet<>(AMap.keySet());
        setKeys.addAll(BMap.keySet());

        List<String> result = new ArrayList<>();
        for (String str : setKeys) {
            if (AMap.getOrDefault(str, 0) + BMap.getOrDefault(str, 0) == 1) {
                result.add(str);
            }
        }

        return result.toArray(new String[0]);
    }
}
