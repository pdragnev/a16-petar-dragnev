package DSA.dsaTasks;

import java.util.*;
import java.util.stream.Collectors;

public class GottaCatchEmAll {
    static class Player implements Comparable<Player> {
        String name;
        String type;
        int age;

        Player(String name, String type, int age) {
            this.name = name;
            this.type = type;
            this.age = age;
        }

        @Override
        public String toString() {
            return String.format("%s(%d)", this.name, this.age);
        }

        @Override
        public int compareTo(Player p) {
            int result = this.name.compareTo(p.name);
            if (result == 0) {
                result = Integer.compare(p.age, this.age);
            }
            return result;
        }
    }

    private static ArrayList<Player> players = new ArrayList<>();
    private static HashMap<String, TreeSet<Player>> playersByType = new HashMap<>();

    private static StringBuilder result = new StringBuilder();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            String[] command = scanner.nextLine().split(" ");
            switch (command[0]) {
                case "add":
                    addPlayer(command);
                    break;
                case "find":
                    findPlayer(command);
                    break;
                case "ranklist":
                    rankList(command);
                    break;
                default:
                    System.out.println(result);
                    return;
            }
        }
    }

    private static void addPlayer(String[] command) {
        Player p = new Player(command[1], command[2], Integer.parseInt(command[3]));
        int position = Integer.parseInt(command[4]);

        players.add(position - 1, p);

        if (!playersByType.containsKey(command[2])) {
            playersByType.put(command[2], new TreeSet<>());
        }

        playersByType.get(command[2]).add(p);

        result.append(
                String.format("Added player %s to position %d\n",
                        command[1],
                        Integer.parseInt(command[4])));
    }

    private static void findPlayer(String[] command) {
        result.append(String.format("Type %s: ", command[1]));

        if (playersByType.containsKey(command[1])) {
            List<String> resultPlayers = playersByType.get(command[1]).stream()
                    .limit(5)
                    .map(Player::toString)
                    .collect(Collectors.toList());

            result.append(String.join("; ", resultPlayers));
        }

        result.append("\n");
    }

    private static void rankList(String[] command) {
        int start = Integer.parseInt(command[1]) - 1;
        int end = Integer.parseInt(command[2]) - 1;

        List<String> rankedPlayers = new ArrayList<>();
        for (int i = start; i <= end; i++) {
            rankedPlayers.add(String.format("%d. %s", i + 1, players.get(i)));
        }

        result.append(String.join("; ", rankedPlayers));
        result.append("\n");
    }
}
/*

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;

public class GottaCatchEmAll {

    private static final String END = "end";

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        List<Pokemon> pokemons = new ArrayList<>();
        String operation = scanner.nextLine();
        while (!END.equals(operation)) {

            String[] operationSplit = operation.split("\\s");
            switch (operationSplit[0]) {
                case "add":
                    int position = Integer.parseInt(operationSplit[4]);
                     Pokemon pokemon = new Pokemon(operationSplit[1],operationSplit[2],Integer.parseInt(operationSplit[3]));
                     pokemons.add(position-1,pokemon);
                    System.out.printf("Added pokemon %s to position %d%n",pokemon.getName(),position);
                    break;
                case "find" :
                        List filteredPokemons= pokemons
                                .stream()
                                .filter(o -> o.getType().equals(operationSplit[1]))
                                .map(Objects::toString)
                                .collect(Collectors.toList());
                    StringBuilder filterResult = new StringBuilder();
                    String start = String.format("Type %s: ",operationSplit[1]);
                    filterResult.append(start);
                    int max = 5;
                    if (filteredPokemons.size() < max){
                        max = filteredPokemons.size();
                    }
                    for (int i = 0; i < max; i++) {
                        filterResult.append(filteredPokemons.get(i).toString()).append("; ");
                    }
                    if (max ==0){
                        System.out.println(filterResult);
                    } else{
                    System.out.println(filterResult.substring(0,filterResult.length()-2));}
                    break;
                case "ranklist" :
                    StringBuilder result = new StringBuilder();
                    int startIndex = Integer.parseInt(operationSplit[1])-1;
                    int endIndex = Integer.parseInt(operationSplit[2]);
                    for (int i = startIndex ; i < endIndex; i++) {
                        result.append(i+1).append(". ").append(pokemons.get(i).toString()).append("; ");
                    }
                    System.out.println(result.substring(0,result.length()-2));
                    break;
                default:
                    break;
            }
            operation = scanner.nextLine();
        }

    }
}

 class Pokemon implements Comparable<Pokemon>{
    private String name;
    private String type;
    private int power;

    public Pokemon(String name, String type, int power) {
        this.name = name;
        this.type = type;
        this.power = power;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public int getPower() {
        return power;
    }

    @Override
    public String toString() {
        return String.format("%s(%d)",getName(),getPower());
    }

    @Override
    public int compareTo(Pokemon pokemon) {
        if (this.name.compareTo(pokemon.name) == 0){
            return pokemon.getPower() - this.getPower();
        }

        return this.name.compareTo(pokemon.name);
    }
}
*/
