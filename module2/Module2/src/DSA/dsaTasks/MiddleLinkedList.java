package DSA.dsaTasks;


public class MiddleLinkedList {
    public ListNode middleNode(ListNode head) {
        ListNode fast = head;
        ListNode slow = head;

        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }
}
/* return slow;
        ArrayList<ListNode> listNodes = new ArrayList<>();

        ListNode temp = head;
        while(temp!=null){
            listNodes.add(temp);
            temp=temp.next;
        }

        return listNodes.get(listNodes.size() / 2);*/