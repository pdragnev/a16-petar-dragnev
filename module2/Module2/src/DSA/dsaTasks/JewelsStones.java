package DSA.dsaTasks;

import java.util.HashSet;
import java.util.Set;

public class JewelsStones {
    public int numJewelsInStones(String J, String S) {

        Set<Character> Jset = new HashSet<>();

        for (Character c : J.toCharArray()) {
            Jset.add(c);
        }

        int count = 0;
        for (Character c : S.toCharArray()) {
            if (Jset.contains(c)) {
                count++;
            }
        }

        return count;
    }
}
