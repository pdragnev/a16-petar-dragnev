package DSA.dsaTasks;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

public class AsteroidCollision {
    public static int[] asteroidCollision(int[] asteroids) {
        Stack<Integer> stack = new Stack();

        asteroids:
        for (int ast : asteroids) {
            while (!stack.isEmpty() && ast < 0 && 0 < stack.peek()) {
                if (-ast < stack.peek() || -ast == stack.pop()) {
                    continue asteroids;
                }
            }
            stack.push(ast);
        }
        return stack.stream().mapToInt(i -> i).toArray();
    }
}
     /*   Stack<Integer> stack = new Stack();

        asteroids:
        for (int ast : asteroids) {
            while (!stack.isEmpty() && ast < 0 && 0 < stack.peek()) {
                if (-ast < stack.peek() || -ast == stack.pop()) {
                    continue asteroids;
                }
            }
            stack.push(ast);
        }
        return stack.stream().mapToInt(i -> i).toArray();
    }*/



       /* List<Integer> list = Arrays.stream(asteroids).boxed().collect(Collectors.toList());

        for (int i = 1; i < list.size(); i++) {
            int tempLeft = list.get(i-1);
            int tempRight = list.get(i);
            if (tempLeft>0 && tempRight<0){
                if (tempLeft > Math.abs(tempRight)) {
                    list.remove(i);
                } else if (tempLeft < Math.abs(tempRight)) {
                        list.remove(i-1);
                    } else {
                        list.remove(i);
                        list.remove(i-1);
                    }
                i=0;
            }
        }

       return  list.stream().mapToInt(i -> i).toArray();

    }*/

