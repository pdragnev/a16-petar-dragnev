package DSA.dsaTasks;

public class ReverseLinkedList {
    public ListNode reverseList(ListNode head) {

        ListNode next;
        ListNode prevous = null;

        while (head != null) {
            next = head.next;
            head.next = prevous;
            prevous = head;
            head = next;
        }
        return prevous;
    }
}
