package DSA.dsaTasks;

import java.util.HashSet;
import java.util.Set;

public class HappyNumbers {
    public static boolean isHappy(int n) {
        int sum = 0;
        Set<Integer> mySet = new HashSet<>();
        while (mySet.add(n)) {
            sum = 0;
            for (String str : String.valueOf(n).split("")) {
                sum = (int) (sum + Math.pow(Integer.parseInt(str), 2));

            }
            n = sum;
            if (n == 1) {
                return true;
            }
        }

        return false;
    }
}
