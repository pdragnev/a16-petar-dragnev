package DSA.dsaTasks;

public class RemoveLinkedListElement {
    public static ListNode removeElements(ListNode head, int val) {
        if (head == null || head.next == null && head.val == val) {
            return null;
        }
        ListNode tempHead = head;
        ListNode prev = new ListNode(0);
        ListNode tempPrev = prev;
        prev.next = head;

        while (tempHead != null) {
            if (tempHead.val == val) {
                tempPrev.next = tempHead.next;
                tempHead = tempHead.next;
                continue;
            }
            tempPrev = tempPrev.next;
            tempHead = tempHead.next;
        }
        return prev.next;
    }
}
