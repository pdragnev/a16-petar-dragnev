package DSA.dsaTasks;

import java.util.Collections;

public class printStars {
    public static void main(String[] args) {

        print(6);
    }

    public static void print(int n) {
        if (n <= 0) {
            return;
        }

        System.out.println(String.join("", Collections.nCopies(n, "*")));
        print(n - 1);
        System.out.println(String.join("", Collections.nCopies(n, "*")));
    }
}
