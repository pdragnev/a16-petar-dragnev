package DSA.dsaTasks;

import java.util.ArrayList;
import java.util.Stack;

public class LemonadeChange {
    public static boolean lemonadeChange(int[] bills) {
        int counter5 = 0, counter10 = 0, counter20 = 0;
        for (int i = 0; i < bills.length; i++) {
            switch (bills[i]) {
                case 5:
                    counter5++;
                    break;
                case 10:
                    counter10++;
                    counter5--;
                    break;
                case 20:
                    counter20++;
                    if (counter10 > 0) {
                        counter10--;
                        counter5--;
                        break;
                    }
                    counter5 -= 3;
                    break;
            }
            if (counter5 < 0) {
                return false;
            }
        }
        return true;
    }
}
