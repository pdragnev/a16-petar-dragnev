package DSA.dsaTasks;

import java.util.*;

public class DoctorsOffice {

    private static final String END = "End";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<String, Integer> map = new HashMap<>();
        List<String> queueList = new LinkedList<>();

        String operation = scanner.nextLine();
        while (!END.equals(operation)) {

            String[] operationSplit = operation.split("\\s");
            switch (operationSplit[0]) {
                case "Append":
                    append(operationSplit[1], map, queueList);
                    System.out.println("OK");
                    break;
                case "Examine":
                    System.out.println(examine(Integer.parseInt(operationSplit[1]), map, queueList));
                    break;
                case "Insert":
                    if (Integer.parseInt(operationSplit[1]) < 0 || Integer.parseInt(operationSplit[1]) > queueList.size()) {
                        System.out.println("Error");
                    } else {
                        insert(operationSplit[2], Integer.parseInt(operationSplit[1]), map, queueList);
                        System.out.println("OK");
                    }
                    break;
                case "Find":
                    System.out.println(find(map, operationSplit[1]));
                    break;
                default:
                    break;
            }
            operation = scanner.nextLine();
        }

    }

    private static void append(String name, Map<String, Integer> map, List<String> queueList) {
        map.put(name, map.getOrDefault(name, 0) + 1);
        queueList.add(name);
    }

    private static String examine(int patientsToExamined, Map<String, Integer> map, List<String> queueList) {
        if (patientsToExamined > queueList.size()) {
            return "Error";
        }
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < patientsToExamined; i++) {
            String examined = queueList.get(0);
            queueList.remove(0);
            result.append(examined).append(" ");
            if (map.get(examined) == 1) {
                map.remove(examined);
            } else {
                map.put(examined, map.get(examined) - 1);
            }
        }
        return result.toString().trim();
    }

    private static int find(Map<String, Integer> map, String name) {
        return map.getOrDefault(name, 0);
    }

    private static void insert(String name, int position, Map<String, Integer> map, List<String> queueList) {
        queueList.add(position, name);
        map.put(name, map.getOrDefault(name, 0) + 1);
    }
}
