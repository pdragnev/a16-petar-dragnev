package DSA.dsaTasks;

import java.util.HashSet;
import java.util.Set;

public class SetMismatch {
    public int[] findErrorNums(int[] nums) {

        int dublicate = 0;
        int missing = 0;
        Set<Integer> mySet = new HashSet<>();

        for (Integer i : nums) {
            if (!mySet.add(i)) {
                dublicate = i;
            }
        }

        for (int i = 1; i <= nums.length; i++) {
            if (!mySet.contains(i)) {
                missing = i;
                break;
            }
        }

        return new int[]{dublicate, missing};
    }
}