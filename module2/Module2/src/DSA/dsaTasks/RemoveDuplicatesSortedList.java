package DSA.dsaTasks;

public class RemoveDuplicatesSortedList {
    public ListNode deleteDuplicates(ListNode head) {

        if (head == null) {
            return null;
        }

        ListNode tempHead = head;

        while (tempHead.next != null) {
            if (tempHead.val == tempHead.next.val) {
                tempHead.next = tempHead.next.next;
            } else {
                tempHead = tempHead.next;
            }
        }

        return head;
    }
}
