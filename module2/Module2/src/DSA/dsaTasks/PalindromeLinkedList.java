package DSA.dsaTasks;

public class PalindromeLinkedList {
    public static boolean isPalindrome(ListNode head) {

        if (head == null) {
            return true;
        }

        ListNode fast = head;
        ListNode slow = head;

        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }

        ListNode prev = null;
        ListNode next;
        ListNode tempHead = slow;
        while (tempHead != null) {
            next = tempHead.next;
            tempHead.next = prev;
            prev = tempHead;
            tempHead = next;
        }

        while (prev != null) {
            if (head.val != prev.val) {
                return false;
            }
            head = head.next;
            prev = prev.next;
        }
        return true;
    }
}
