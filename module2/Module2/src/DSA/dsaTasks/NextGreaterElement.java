package DSA.dsaTasks;

public class NextGreaterElement {
    public static int[] nextGreaterElement(int[] nums1, int[] nums2) {

        int[] res = new int[nums1.length];
        boolean doesMatch = false;
        boolean isGreaterFound = false;
        for (int i = 0; i < nums1.length; i++) {

            for (int value : nums2) {
                if (nums1[i] == value || doesMatch) {
                    doesMatch = true;
                    if (value > nums1[i]) {
                        res[i] = value;
                        isGreaterFound = true;
                        break;
                    }
                }
            }
            if (!isGreaterFound) {
                res[i] = -1;
            }
            doesMatch = false;
            isGreaterFound = false;
        }
        return res;
    }

}
