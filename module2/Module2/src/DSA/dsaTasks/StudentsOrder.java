package DSA.dsaTasks;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class StudentsOrder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] namesChangesNum = scanner.nextLine().split(" ");
        LinkedList<String> names = new LinkedList<>(Arrays.asList(scanner.nextLine().split(" ")));

        for (int i = 0; i < Integer.parseInt(namesChangesNum[1]); i++) {
            String[] input = scanner.nextLine().split(" ");
            String nameMove = input[0];
            String nameBefore = input[1];
            names.remove(nameMove);
            names.add(names.indexOf(nameBefore), nameMove);
        }

        System.out.println(Arrays.toString(names.toArray()).replace("[", "")
                .replace("]", "").replace(",", ""));
    }
}
