package DSA.dsaTasks;

import java.util.*;

public class FirstPairSumLenN {
    public static void main(String[] args) {
        int target = 13;
        Integer[] colection = {12, 2, 6, 14, 8, 1, 5, 3, 12, 5};

        Set<Integer> mySet = new HashSet<>(Arrays.asList(colection));

        for (Integer item : colection) {
            if (mySet.contains(target - item)) {
                System.out.printf("(%d,%d)", item, target - item);
                break;
            }
        }
    }
}
