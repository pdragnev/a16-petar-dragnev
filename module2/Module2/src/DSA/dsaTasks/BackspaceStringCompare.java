package DSA.dsaTasks;

import java.util.Stack;

public class BackspaceStringCompare {
    /*  Input: S = "ab#c", T = "ad#c"
      Output: true
      Explanation: Both S and T become "ac".*/
    public static boolean backspaceCompare(String S, String T) {
        return buildString(S).equals(buildString(T));
    }

    private static String buildString(String str) {
        Stack<Character> stack = new Stack<>();
        char[] chars = str.toCharArray();
        for (char c : chars) {
            stack.push(c);
            if (stack.peek() == '#') {
                stack.pop();
                if (!stack.isEmpty()) {
                    stack.pop();
                }
            }
        }
        return stack.toString();
    }
}
