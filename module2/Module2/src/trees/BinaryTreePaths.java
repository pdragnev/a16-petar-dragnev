package trees;

import java.util.ArrayList;
import java.util.List;

public class BinaryTreePaths {
    public static List<String> binaryTreePaths(TreeNode root) {
        List<String> current = new ArrayList<>();
        List<String> all = new ArrayList<>();
        treePathHelper(root, current, all);

        return all;
    }

    private static void treePathHelper(TreeNode root, List<String> current, List<String> all) {
        if (root == null) {
            return;
        }
        if (root.left == null && root.right == null) {
            current.add(String.valueOf(root.val));
            all.add(current.toString()
                    .replace(", ", "->")
                    .replace("[", "")
                    .replace("]", "")
                    .replace(" ", ""));
            current.remove(current.size() - 1);
            return;
        }

        current.add(String.valueOf(root.val));

        treePathHelper(root.left, current, all);
        treePathHelper(root.right, current, all);

        current.remove(current.size() - 1);
    }
}
