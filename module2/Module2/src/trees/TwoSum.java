package trees;

import java.util.ArrayList;
import java.util.List;

public class TwoSum {
    public boolean findTarget(TreeNode root, int k) {
        List<Integer> result = new ArrayList<>();
        findTargetHelper(root, result);
        for (int i = 0; i < result.size(); i++) {
            for (int j = 0; j < result.size(); j++) {
                if (i != j && result.get(i) + result.get(j) == k) {
                    return true;
                }
            }
        }
        return false;
    }

    private void findTargetHelper(TreeNode node, List<Integer> result) {
        if (node == null) {
            return;
        }
        findTargetHelper(node.left, result);
        result.add(node.val);
        findTargetHelper(node.right, result);
    }
}
