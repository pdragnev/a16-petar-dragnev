package com.company;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class DoublyLinkedListImpl<T> implements DoublyLinkedList<T> {


    private DoubleNode<T> head;
    private DoubleNode<T> tail;
    private int count = 0;

    public DoublyLinkedListImpl() {
    }

    public DoublyLinkedListImpl(List<T> values) {
        for (T value : values) {
            addLast(value);
        }
    }

    @Override
    public DoubleNode getHead() {
        return head;
    }

    @Override
    public DoubleNode getTail() {
        return tail;
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public void addLast(T value) {
        DoubleNode<T> addNode = new DoubleNode<>(value, tail, null);
        if (isEmpty()) {
            head = addNode;
            tail = addNode;
            head.setNext(tail);
            tail.setPrev(head);
            addNode.setPrev(null);
            count++;
            return;
        }
        tail.setNext(addNode);
        tail = addNode;
        count++;
    }

    @Override
    public void addFirst(T value) {
        DoubleNode<T> addNode = new DoubleNode<>(value, null, head);
        if (isEmpty()) {
            head = addNode;
            tail = addNode;
            head.setNext(tail);
            tail.setPrev(head);
            addNode.setNext(null);
            count++;
            return;
        }
        head.setPrev(addNode);
        head = addNode;
        count++;
    }

    @Override
    public void insertBefore(DoubleNode node, T value) {
        if (node == null) {
            throw new NullPointerException();
        }
        DoubleNode<T> newNode = new DoubleNode(value, null, node);
        if (head != node) {
            newNode.setPrev(node.getPrev());
            node.getPrev().setNext(newNode);
            node.setPrev(newNode);
            count++;
        } else {
            addFirst(value);
        }
    }

    @Override
    public void insertAfter(DoubleNode node, T value) {
        if (node == null) {
            throw new NullPointerException();
        }
        DoubleNode<T> newNode = new DoubleNode<>(value, node, null);
        if (tail != node) {
            newNode.setNext(node.getNext());
            node.getNext().setPrev(newNode);
            node.setNext(newNode);
            count++;
        } else {
            addLast(value);
        }
    }

    @Override
    public T removeFirst() {
        checkEmpty();
        DoubleNode<T> newHead = head.getNext();
        DoubleNode<T> currentHead = head;
        currentHead.setNext(null);
        head = newHead;
        if (head != null) {
            head.setPrev(null);
        }
        count--;

        return currentHead.getValue();
    }

    @Override
    public T removeLast() {
        checkEmpty();
        DoubleNode<T> newTail = tail.getPrev();
        DoubleNode<T> currentTail = tail;
        currentTail.setPrev(null);
        tail = newTail;
        if (tail != null) {
            tail.setNext(null);
        }
        count--;
        return currentTail.getValue();
    }


    @Override
    public DoubleNode find(T value) {
        if (isEmpty()) {
            return null;
        }
        if (head.getValue().equals(value)) {
            return head;
        } else {
            DoubleNode temp = head.getNext();
            while (temp != null) {
                if (temp.getValue().equals(value)) {
                    return temp;
                }
                temp = temp.getNext();
            }
        }
        return null;
    }

    @Override
    public Iterator<T> iterator() {
        return new ListIterator<T>() {
            DoubleNode currentNode = getHead();

            @Override
            public boolean hasNext() {
                return currentNode != null;
            }

            @Override
            public T next() {
                T value = (T) currentNode.getValue();
                currentNode = currentNode.getNext();
                return value;
            }

            @Override
            public boolean hasPrevious() {
                return false;
            }

            @Override
            public T previous() {
                return null;
            }

            @Override
            public int nextIndex() {
                return 0;
            }

            @Override
            public int previousIndex() {
                return 0;
            }

            @Override
            public void remove() {

            }

            @Override
            public void set(T t) {

            }

            @Override
            public void add(T t) {

            }
        };
    }

    private boolean isEmpty() {
        return count == 0;
    }

    private void checkEmpty() {
        if (isEmpty()) {
            throw new NullPointerException();
        }
    }
}
