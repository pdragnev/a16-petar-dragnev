package com.telerikacademy;

import java.util.Arrays;

public class StackImpl<T> implements Stack<T> {

    private T[] stackArray;
    private int top;


    public StackImpl() {
        this.stackArray = (T[]) new Object[10];
        this.top = -1;
    }

    @Override
    public String toString() {
        return Arrays.toString(stackArray);
    }

    @Override
    public void push(T elem) {
        if (top + 1 == stackArray.length) {
            resize();
        }
        top++;
        stackArray[top] = elem;
    }

    @Override
    public T pop() {
        if (isEmpty()) {
            throw new IllegalArgumentException();
        }
        T popValue = stackArray[top];
        stackArray[top] = null;
        top--;

        return popValue;
    }

    @Override
    public T peek() {
        if (isEmpty()) {
            throw new IllegalArgumentException();
        }
        return stackArray[top];
    }

    @Override
    public int size() {
        return top + 1;
    }

    @Override
    public boolean isEmpty() {
        return stackArray[0] == null;
    }

    private void resize() {
        stackArray = Arrays.copyOf(stackArray, stackArray.length * 2);
    }
}
