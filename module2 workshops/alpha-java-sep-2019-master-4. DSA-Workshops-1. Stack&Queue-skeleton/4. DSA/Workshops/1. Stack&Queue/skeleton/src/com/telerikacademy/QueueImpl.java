package com.telerikacademy;

public class QueueImpl<T> implements Queue<T> {

    private Node head;

    private int size=0;



    @Override
    public void offer(T elem) {
        Node newNode = new Node(elem);
        if (head == null) {
            head = newNode;
        } else {
            newNode.setNext(head);
            head = newNode;
        }
        size++;
    }

    @Override
    public T poll() {
        if (isEmpty()) {
            throw new IllegalArgumentException();
        }
        Node temp = head;
        Node nextTemp = head.getNext();
        if (temp.getNext() != null) {
            while (nextTemp.getNext() != null) {
                temp = nextTemp;
                nextTemp = nextTemp.getNext();
            }
        } else {
            head.setNext(null);
            size--;
            return (T) nextTemp.getData();
        }

        temp.setNext(null);
        size--;

        return (T) nextTemp.getData();
    }

    @Override
    public boolean isEmpty() {
        return head==null;
    }

    @Override
    public T peek() {
        if (isEmpty()){
            throw new IllegalArgumentException();
        }
        Node temp = head;
        while(temp.getNext()!=null){
            temp=temp.getNext();
        }
        return (T) temp.getData();
    }

    @Override
    public int size() {
        return size;
    }
}
