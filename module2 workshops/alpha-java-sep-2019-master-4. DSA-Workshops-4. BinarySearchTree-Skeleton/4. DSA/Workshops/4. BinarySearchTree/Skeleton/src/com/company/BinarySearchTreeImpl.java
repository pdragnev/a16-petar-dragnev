package com.company;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class BinarySearchTreeImpl implements BinarySearchTree {

    private BinaryTreeNode root;

    @Override
    public BinaryTreeNode getRoot() {
        return root;
    }

    @Override
    public void insert(int value) {
        if (getRoot() == null) {
            root = new BinaryTreeNode(value);
            return;
        }
        BinaryTreeNode temp = root;
        BinaryTreeNode previous = root;

        while (temp != null) {
            if (value < temp.getValue()) {
                previous = temp;
                temp = temp.getLeftChild();
            } else {
                previous = temp;
                temp = temp.getRightChild();
            }
        }
        if (value < previous.getValue()) {
            previous.setLeftChild(new BinaryTreeNode(value));
        } else {
            previous.setRightChild(new BinaryTreeNode(value));
        }
    }

    @Override
    public BinaryTreeNode search(int value) {
        if (getRoot() == null) {
            return null;
        }
        List<BinaryTreeNode> nodes = new ArrayList<>();
        return search(root, value, nodes);
    }

    @Override
    public List<Integer> inOrder() {
        List<Integer> result = new ArrayList<>();
        inorderRecord(root, result);
        return result;
    }

    @Override
    public List<Integer> postOrder() {
        List<Integer> result = new ArrayList<>();
        postOrderRecord(root, result);
        return result;
    }

    @Override
    public List<Integer> preOrder() {
        List<Integer> result = new ArrayList<>();
        preOrderRecord(root, result);
        return result;
    }

    @Override
    public List<Integer> bfs() {
        List<Integer> result = new ArrayList<>();
        Queue<BinaryTreeNode> nodes = new ArrayDeque<>();
        if (getRoot() != null) {
            nodes.offer(root);
        }
        while (!nodes.isEmpty()) {
            BinaryTreeNode temp = nodes.poll();
            result.add(temp.getValue());

            if (temp.getLeftChild() != null) {
                nodes.offer(temp.getLeftChild());
            }
            if (temp.getRightChild() != null) {
                nodes.offer(temp.getRightChild());
            }
        }

        return result;
    }

    @Override
    public int height() {
        return getHeight(root);
    }

    private int getHeight(BinaryTreeNode node) {
        if (node == null) {
            return -1;
        }
        return Math.max(getHeight(node.getLeftChild()), getHeight(node.getRightChild())) + 1;
    }

    private void inorderRecord(BinaryTreeNode node, List<Integer> result) {
        if (node != null) {
            inorderRecord(node.getLeftChild(), result);
            result.add(node.getValue());
            inorderRecord(node.getRightChild(), result);
        }
    }

    private void postOrderRecord(BinaryTreeNode node, List<Integer> result) {
        if (node != null) {
            postOrderRecord(node.getLeftChild(), result);
            postOrderRecord(node.getRightChild(), result);
            result.add(node.getValue());
        }
    }

    private void preOrderRecord(BinaryTreeNode node, List<Integer> result) {
        if (node != null) {
            result.add(node.getValue());
            preOrderRecord(node.getLeftChild(), result);
            preOrderRecord(node.getRightChild(), result);
        }
    }

    private BinaryTreeNode search(BinaryTreeNode node, int value, List<BinaryTreeNode> nodes) {
        if (node == null) {
            return null;
        }
        if (node.getValue() == value) {
            nodes.add(node);
            return node;
        }
        if (value < node.getValue()) {
            search(node.getLeftChild(), value, nodes);
        } else {
            search(node.getRightChild(), value, nodes);
        }

        return nodes.isEmpty() ? null : nodes.get(0);
    }
}
