package com.company;

public class Main {

    public static void main(String[] args) {
        BinarySearchTree tree = new BinarySearchTreeImpl();
        tree.insert(10);
        tree.insert(5);
        tree.insert(15);
        tree.insert(3);
        tree.insert(7);

        tree.insert(18);
        System.out.println(tree.bfs());
        //System.out.println(RangeSumBST.rangeSumBST(tree.getRoot(),7,15));
        System.out.println(BinaryTreePaths.binaryTreePaths(tree.getRoot()).toString());


    }

}
