package com.company;

public class RangeSumBST {
    public static int rangeSumBST(BinaryTreeNode root, int L, int R) {

        if (root == null) {
            return 0;
        }
        return ((root.getValue() < L || root.getValue() > R) ? 0 : root.getValue()) + rangeSumBST(root.getLeftChild(), L, R)
                + rangeSumBST(root.getRightChild(), L, R);
    }
}
