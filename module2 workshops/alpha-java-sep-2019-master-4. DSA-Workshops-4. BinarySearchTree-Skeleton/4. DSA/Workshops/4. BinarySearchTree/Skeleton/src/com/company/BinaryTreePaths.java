package com.company;

import java.util.ArrayList;
import java.util.List;

public class BinaryTreePaths {
    public static List<String> binaryTreePaths(BinaryTreeNode root) {
        List<String> current = new ArrayList<>();
        List<String> all = new ArrayList<>();
        treePathHelper(root, current, all);

        return all;
    }

    private static void treePathHelper(BinaryTreeNode root, List<String> current, List<String> all) {
        if (root == null) {
            return;
        }
        if (root.getLeftChild() == null && root.getRightChild() == null) {
            current.add(String.valueOf(root.getValue()));
            all.add(current.toString()
                    .replace(", ", "->")
                    .replace("[", "")
                    .replace("]", "")
                    .replace(" ", ""));
            current.remove(current.size() - 1);
            return;
        }

        current.add(String.valueOf(root.getValue()));

        treePathHelper(root.getLeftChild(), current, all);
        treePathHelper(root.getRightChild(), current, all);

        current.remove(current.size() - 1);
    }
}
