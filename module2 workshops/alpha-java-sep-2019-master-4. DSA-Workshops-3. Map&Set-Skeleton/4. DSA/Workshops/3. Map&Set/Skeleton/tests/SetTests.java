import com.telerikacademy.set.MyHashSet;
import com.telerikacademy.set.MyHashSetImpl;
import org.junit.Assert;
import org.junit.Test;

public class SetTests {
    
    @Test
    public void add_ShouldNotAdd_IfElementAlreadyInSet() {
        //Arrange
        MyHashSet<String> set = new MyHashSetImpl<>();
        String element = "test";
        
        //Act
        set.add(element);
        set.add(element);
        
        //Assert
        Assert.assertEquals(1, set.size());
        Assert.assertTrue(set.contains(element));
    }
    
    @Test
    public void add_ShouldAdd_IfElementNotInSet() {
        //Arrange
        MyHashSet<String> set = new MyHashSetImpl<>();
        String element1 = "test1";
        String element2 = "test2";
        
        //Act
        set.add(element1);
        set.add(element2);
        
        //Assert
        Assert.assertEquals(2, set.size());
        Assert.assertTrue(set.contains(element1));
        Assert.assertTrue(set.contains(element2));
    }
    
    @Test
    public void add_ShouldIncreaseSize_AfterSuccessfulAddition() {
        //Arrange
        MyHashSet<String> set = new MyHashSetImpl<>();
        int initialSize = set.size();
        String element = "test";
        
        //Act
        set.add(element);
        
        //Assert
        Assert.assertEquals(initialSize + 1, set.size());
    }
    
    @Test
    public void add_ShouldNotIncreaseSize_AfterUnsuccessfulAddition() {
        //Arrange
        MyHashSet<String> set = new MyHashSetImpl<>();
        String element = "test";
        set.add(element);
        int initialSize = set.size();
        
        //Act
        set.add(element);
        
        //Assert
        Assert.assertEquals(initialSize, set.size());
    }
    
    @Test
    public void add_ShouldResize_IfSizeIsOverThreshold() {
        //Arrange
        MyHashSet<String> set = new MyHashSetImpl<>();
        int initialCapacity = set.capacity();
        
        //Act
        for (int i = 0; i < 12; i++) {
            set.add(String.format("test%d", i));
        }
        
        //Assert
        Assert.assertNotEquals(initialCapacity, set.capacity());
        Assert.assertEquals(set.size(), 12);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void remove_ShouldThrow_IfValueNotInSet() {
        //Arrange
        MyHashSet<String> set = new MyHashSetImpl<>();
        //Act
        set.remove("test");
    }
    
    @Test
    public void remove_ShouldRemoveElement_IfInSet() {
        //Arrange
        MyHashSet<String> set = new MyHashSetImpl<>();
        String element = "test";
        set.add(element);
        
        //Act
        set.remove(element);
        
        //Assert
        Assert.assertFalse(set.contains(element));
    }
    
    @Test
    public void remove_ShouldDecrementSize_IdRemoveSuccessful() {
        //Arrange
        MyHashSet<String> set = new MyHashSetImpl<>();
        String element = "test";
        set.add(element);
        int initialValue = set.size();
        
        //Act
        set.remove(element);
        
        //Assert
        Assert.assertEquals(initialValue - 1, set.size());
    }
    
    @Test
    public void remove_ShouldNotDecrementSize_IdRemoveUnsuccessful() {
        //Arrange
        MyHashSet<String> set = new MyHashSetImpl<>();
        int initialValue = set.size();
        
        //Act
        try {
            set.remove("test");
        } catch (IllegalArgumentException e) {
        
        }
        
        //Assert
        Assert.assertEquals(initialValue, set.size());
    }
    
    @Test
    public void contains_ShouldReturnTrue_IfSetContainsValue() {
        //Arrange
        MyHashSet<String> set = new MyHashSetImpl<>();
        String element = "test";
        set.add(element);
        
        //Act & Assert
        Assert.assertTrue(set.contains(element));
    }
    
    @Test
    public void contains_ShouldReturnFalse_IfSetDoesNotContainValue() {
        //Arrange
        MyHashSet<String> set = new MyHashSetImpl<>();
        
        //Act & Assert
        Assert.assertFalse(set.contains("test"));
    }
    
}
