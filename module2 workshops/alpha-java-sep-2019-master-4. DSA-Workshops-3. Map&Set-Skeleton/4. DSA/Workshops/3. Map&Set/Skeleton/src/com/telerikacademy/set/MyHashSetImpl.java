package com.telerikacademy.set;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MyHashSetImpl<T> implements MyHashSet<T> {
    private static final double LOAD_FACTOR = 0.75d;
    private int capacity = 16;
    private int size = 0;

    private List<T>[] buckets = new ArrayList[capacity];

    @Override
    public boolean add(T value) {
        if (contains(value)) {
            return false;
        }
        List<T> bucketValue = getBucket(value);
        bucketValue.add(value);
        size++;
        resize();
        return true;
    }

    @Override
    public void remove(T value) {
        if (!contains(value)){
            throw new IllegalArgumentException("Not in this set");
        }
        List<T> bucketValue = getBucket(value);
        bucketValue.remove(value);
        size--;
    }

    @Override
    public boolean contains(T value) {
        List<T> bucketValue = getBucket(value);
        return bucketValue.contains(value);
    }

    @Override
    public int capacity() {
        return capacity;
    }

    @Override
    public int size() {
        return size;
    }

    private int index(T value) {
        return Math.abs(Objects.hashCode(value)) % buckets.length;
    }

    private void resize() {
        if (size < capacity * LOAD_FACTOR) {
            return;
        }

        List<T>[] tempBuckets = buckets;

        capacity *= 2;
        size = 0;
        buckets = new ArrayList[capacity];

        for (List<T> bucket : tempBuckets) {
            if (bucket != null) {
                for (T item : bucket) {
                    add(item);
                }
            }
        }
    }

    private List<T> getBucket(T value) {
        int indexValue = index(value);
        if (buckets[indexValue] == null) {
            buckets[indexValue] = new ArrayList<>();
        }
        return buckets[indexValue];
    }
}
