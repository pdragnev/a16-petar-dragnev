package com.telerikacademy.set;

public interface MyHashSet<T> {
    boolean add(T value);

    void remove(T value);

    int size();

    boolean contains(T value);
    
    int capacity();
}
