package com.telerikacademy;

import com.telerikacademy.set.MyHashSet;
import com.telerikacademy.set.MyHashSetImpl;

public class Main {

    public static void main(String[] args) {

        MyHashSet<String> set = new MyHashSetImpl<>();

        set.add("test");
        set.add("test1");

        System.out.println(set.size());

        set.add("test1");
        System.out.println(set.size());
    }
}
