package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.ValidationHelper;
import com.telerikacademy.cosmetics.models.common.GenderType;

public class Product {
    private static final int MIN__PRODUCT_NAME_LENGTH = 3;
    private static final int MAX__PRODUCT_NAME_LENGTH = 10;
    private static final int MIN__BRAND_NAME_LENGTH = 2;
    private static final int MAX__BRAND_NAME_LENGTH = 10;
    private double price;
    private String name;
    private String brand;
    private GenderType gender;

    public Product(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        setGender(gender);
    }

    public double getPrice() {
        return price;
    }

    private void setPrice(double price) {
        ValidationHelper.CheckPrice(price);
        this.price = price;
    }

    private void setName(String name) {
        ValidationHelper.CheckStringLength(name, MIN__PRODUCT_NAME_LENGTH, MAX__PRODUCT_NAME_LENGTH);
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    private void setBrand(String brand) {
        ValidationHelper.CheckStringLength(brand, MIN__BRAND_NAME_LENGTH, MAX__BRAND_NAME_LENGTH);
        this.brand = brand;
    }

    public GenderType getGender() {
        return gender;
    }

    private void setGender(GenderType gender) {
        this.gender = gender;
    }

    public String print() {
        return String.format("#%s %s" + System.lineSeparator() +
                "#Price: %.2f" + System.lineSeparator() +
                "#Gender: %s" + System.lineSeparator() +
                "===", name, brand, price, gender);
        // Format:
        //" #[Name] [Brand]
        // #Price: [Price]
        // #Gender: [Gender]
        // ==="
    }
}
