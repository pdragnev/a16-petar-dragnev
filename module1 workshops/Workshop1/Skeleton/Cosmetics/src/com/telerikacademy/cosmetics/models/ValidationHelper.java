package com.telerikacademy.cosmetics.models;

import com.telerikacademy.cosmetics.models.products.Product;

public class ValidationHelper {
    private static final String PRODUCT_IS_NULL_MSG = "Product is null";
    private static final String WRONG_STRING_LEN_FORMAT = "The length must be between %d and %d!";
    private static final String INVALID_PRICE = "Price is negative number!";

    public static void isProductNull(Product product) {
        if (product == null) {
            throw new IllegalArgumentException(PRODUCT_IS_NULL_MSG);
        }
    }

    public static void CheckStringLength(String str, int minValue, int maxValue) {
        String msg = String.format(WRONG_STRING_LEN_FORMAT, minValue, maxValue);
        if (str.length() < minValue || str.length() > maxValue) {
            throw new IllegalArgumentException(msg);
        }
    }

    public static void CheckPrice(double price) {
        if (price <= 0) {
            throw new IllegalArgumentException(INVALID_PRICE);
        }
    }
}


