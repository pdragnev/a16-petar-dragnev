package com.telerikacademy.cosmetics.models.cart;

import com.telerikacademy.cosmetics.models.ValidationHelper;
import com.telerikacademy.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    private List<Product> productList;

    public ShoppingCart() {
        productList = new ArrayList<>();
    }

    public List<Product> getProductList() {
        return new ArrayList<>(productList);
    }

    public void addProduct(Product product) {
        ValidationHelper.isProductNull(product);
        productList.add(product);
    }

    public void removeProduct(Product product) {
        ValidationHelper.isProductNull(product);
        productList.remove(product);
    }

    public boolean containsProduct(Product product) {
        ValidationHelper.isProductNull(product);
        if (productList.contains(product)) {
            return true;
        } else {
            return false;
        }
    }

    public double totalPrice() {
        double totalPrice = 0;
        for (Product product : productList) {
            totalPrice += product.getPrice();
        }

        return totalPrice;
    }
}
