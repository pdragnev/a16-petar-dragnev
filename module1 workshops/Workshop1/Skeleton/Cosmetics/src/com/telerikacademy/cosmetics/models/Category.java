package com.telerikacademy.cosmetics.models;

import com.telerikacademy.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.List;

public class Category {
    private static final int MIN_CATEGORY_NAME_LENGHT = 2;
    private static final int MAX_CATEGORY_NAME_LENGHT = 15;
    private String name;
    private List<Product> products;

    public Category(String name) {
        setName(name);
        products = new ArrayList<>();
    }

    public void setName(String name) {
        if (name.length() < MIN_CATEGORY_NAME_LENGHT || name.length() > MAX_CATEGORY_NAME_LENGHT) {
            throw new IllegalArgumentException("Wrong category name!");
        }
        this.name = name;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void addProduct(Product product) {
        ValidationHelper.isProductNull(product);
        products.add(product);
    }


    public void removeProduct(Product product) {
        if (!products.contains(product)) {
            throw new IllegalArgumentException("Product to remove not found in list!");
        }
        ValidationHelper.isProductNull(product);
        products.remove(product);
    }

    public String print() {
        StringBuilder productInfo = new StringBuilder();
        if (products.isEmpty()) {
            productInfo.append("#No product in this category");
        } else {
            for (Product product : products) {
                productInfo.append(product.print());
            }
        }
        String result = productInfo.toString().replace("#", " #").replace("===", " ===");
        result = String.format("#Category: %s\n", name) + result;
        return result;
    }
}
