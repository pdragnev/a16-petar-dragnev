package com.telerikacademy.agency.models.enums;

public enum VehicleType {
    LAND,
    AIR,
    SEA;

    @Override
    public String toString() {
        switch (this){
            case LAND:return "Land";
            case AIR:return "Air";
            case SEA:return "Sea";
        }
        throw new IllegalArgumentException();
    }
}
