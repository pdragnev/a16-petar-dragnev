package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.contracts.Bus;
import com.telerikacademy.agency.models.enums.VehicleType;

public class BusImpl extends VehicleImpl implements Bus {

    private static final int BUS_MIN_PASSENGERS = 10;
    private static final int BUS_MAX_PASSENGERS = 50;
    private static final String ERROR_PASSENGER_CAPACITY = String.format("A bus cannot have less than %d passengers or more than %d passengers."
            ,BUS_MIN_PASSENGERS,BUS_MAX_PASSENGERS);
    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer);
    }

    @Override
    public VehicleType getVehicleType() {
        return VehicleType.LAND;
    }

    @Override
    public String toString() {
        return String.format("Bus ----%n%s", super.toString());
    }

    @Override
    protected int getMinPassengers() {
        return BUS_MIN_PASSENGERS;
    }

    @Override
    protected int getMaxPassengers() {
        return BUS_MAX_PASSENGERS;
    }

    @Override
    protected String getErrorPassengersCapacity() {
        return ERROR_PASSENGER_CAPACITY;
    }
}
