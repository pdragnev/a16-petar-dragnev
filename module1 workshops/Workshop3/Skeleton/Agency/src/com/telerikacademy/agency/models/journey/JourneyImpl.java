package com.telerikacademy.agency.models.journey;

import com.telerikacademy.agency.models.ValidationHelper;
import com.telerikacademy.agency.models.contracts.Journey;
import com.telerikacademy.agency.models.contracts.Vehicle;

public class JourneyImpl implements Journey {
    private static final int MIN_LEN_START_LOCATION = 5;
    private static final int MAX_LEN_START_LOCATION = 25;
    private static final String STARTING_LOCATION_ERROR = String.format("The StartingLocation's length cannot be less than %d or more than %d symbols long."
            ,MIN_LEN_START_LOCATION,MAX_LEN_START_LOCATION);

    private static final int MIN_LEN_DESTINATION = 5;
    private static final int MAX_LEN_DESTINATION = 25;
    private static final String DESTINATION_ERROR = String.format("The Destination's length cannot be less than %d or more than %d symbols long."
            ,MIN_LEN_DESTINATION,MAX_LEN_DESTINATION);

    private static final int MAX_DISTANCE = 5000;
    private static final int MIN_DISTANCE = 5;
    private static final String ERROR_DISTANCE = String.format("The Distance cannot be less than %d or more than %d kilometers.",MIN_DISTANCE,MAX_DISTANCE);

    private static final String START_LOCATION_IS_NULL = "StartLocation is null";
    private static final String DESTINATION_IS_NULL = "Destination is null";
    private static final String VEHICLE_CAN_T_BE_NULL = "Vehicle can't be null";

    private String startLocation;
    private String destination;
    private int distance;
    private Vehicle vehicle;

    public JourneyImpl(String startLocation, String destination, int distance, Vehicle vehicle) {
        setStartLocation(startLocation);
        setDestination(destination);
        setDistance(distance);
        setVehicle(vehicle);
    }

    @Override
    public String getStartLocation() {
        return startLocation;
    }

    private void setStartLocation(String startLocation) {
        ValidationHelper.checkObjectNotNull(startLocation, START_LOCATION_IS_NULL);
        ValidationHelper.checkStringLength(startLocation, MIN_LEN_START_LOCATION, MAX_LEN_START_LOCATION, STARTING_LOCATION_ERROR);
        this.startLocation = startLocation;
    }

    @Override
    public String getDestination() {
        return destination;
    }

    private void setDestination(String destination) {
        ValidationHelper.checkObjectNotNull(destination, DESTINATION_IS_NULL);
        ValidationHelper.checkStringLength(destination, MIN_LEN_DESTINATION, MAX_LEN_DESTINATION,DESTINATION_ERROR);
        this.destination = destination;
    }

    @Override
    public int getDistance() {
        return distance;
    }

    private void setDistance(int distance) {
        ValidationHelper.checkValueInRange(distance, MIN_DISTANCE, MAX_DISTANCE,ERROR_DISTANCE);
        this.distance = distance;
    }

    @Override
    public Vehicle getVehicle() {
        return vehicle;
    }

    private void setVehicle(Vehicle vehicle) {
        ValidationHelper.checkObjectNotNull(vehicle, VEHICLE_CAN_T_BE_NULL);
        this.vehicle = vehicle;
    }

    @Override
    public double calculateTravelCosts() {
        return vehicle.getPricePerKilometer()*distance;
    }

    @Override
    public String toString() {
        return String.format("Journey ----%n" +
                "Start location: %s%n" +
                "Destination: %s%n" +
                "Distance: %d%n" +
                "Vehicle type: %s%n" +
                "Travel costs: %.2f%n",getStartLocation(),getDestination(),getDistance(),getVehicle().getVehicleType(),calculateTravelCosts());
    }
}
