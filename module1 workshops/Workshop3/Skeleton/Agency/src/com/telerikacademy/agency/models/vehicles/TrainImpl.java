package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.ValidationHelper;
import com.telerikacademy.agency.models.contracts.Train;
import com.telerikacademy.agency.models.enums.VehicleType;

public class TrainImpl extends VehicleImpl implements Train {

    private static final int MIN_CART_CAPACITY = 1;
    private static final int MAX_CART_CAPACITY = 15;
    private static final String ERROR_CARTS = String.format("A train cannot have less than %d cart or more than %d carts.",MIN_CART_CAPACITY,MAX_CART_CAPACITY);
    private static final int TRAIN_MIN_PASSENGERS = 30;
    private static final int TRAIN_MAX_PASSENGERS = 150;
    private static final String ERROR_PASSENGER_CAPACITY = String.format("A train cannot have less than %d passengers or more than %d passengers."
            ,TRAIN_MIN_PASSENGERS,TRAIN_MAX_PASSENGERS);
    private int carts;

    public TrainImpl(int passengerCapacity, double pricePerKilometer, int carts) {
        super(passengerCapacity, pricePerKilometer);
        setCarts(carts);
    }

    @Override
    public int getCarts() {
        return carts;
    }

    private void setCarts(int carts) {
        ValidationHelper.checkValueInRange(carts, MIN_CART_CAPACITY, MAX_CART_CAPACITY,ERROR_CARTS);
        this.carts = carts;
    }

    @Override
    public VehicleType getVehicleType() {
        return VehicleType.LAND;
    }

    @Override
    public String toString() {
        return String.format("Train ----%n%s" +
                            "Carts amount: %d%n",super.toString(),getCarts());
    }

    @Override
    protected int getMinPassengers() {
        return TRAIN_MIN_PASSENGERS;
    }

    @Override
    protected int getMaxPassengers() {
        return TRAIN_MAX_PASSENGERS;
    }

    @Override
    protected String getErrorPassengersCapacity() {
        return ERROR_PASSENGER_CAPACITY;
    }
}
