package com.telerikacademy.agency.models;

public class ValidationHelper {

    public static void checkValueInRange(double value, double min, double max, String errorMessage) {
        if (value < min || value > max) {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    public static void checkStringLength(String value, int min, int max, String errorMessage) {
        checkValueInRange(value.length(), min, max, errorMessage);
    }

    public static void checkObjectNotNull(Object value, String errorMessage) {
        if (value == null) {
            throw new IllegalArgumentException(errorMessage);
        }
    }
}
