package com.telerikacademy.agency.models.contracts;

public interface Airplane extends Vehicle {
    boolean hasFreeFood();
}
