package com.telerikacademy.agency.models.contracts;

public interface Journey {
    String getDestination();

    int getDistance();

    String getStartLocation();

    Vehicle getVehicle();

    double calculateTravelCosts();
}