package com.telerikacademy.agency.models.contracts;

public interface Train extends Vehicle {
    int getCarts();
}
