package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.ValidationHelper;
import com.telerikacademy.agency.models.contracts.Vehicle;
import com.telerikacademy.agency.models.enums.VehicleType;

abstract public class VehicleImpl implements Vehicle {

    private static final int MIN_PASSENGER_CAPACITY = 1;
    private static final int MAX_PASSENGER_CAPACITY = 800;
    private static final double MIN_PRICE_VALUE = 0.1;
    private static final double MAX_PRICE_VALUE = 2.5;
    private static final String ERROR_PASSENGER_CAPACITY =String.format("A vehicle with less than %d passengers or more than %d passengers cannot exist!"
            ,MIN_PASSENGER_CAPACITY,MAX_PASSENGER_CAPACITY);
    private static final String ERROR_PRICE_PER_KILOMETER = String.format("A vehicle with a price per kilometer lower than $%.2f or higher than $%.2f cannot exist!",
            MIN_PRICE_VALUE,MAX_PRICE_VALUE);
    private int passengerCapacity;
    private double pricePerKilometer;

    VehicleImpl(int passengerCapacity, double pricePerKilometer) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
    }

    @Override
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    private void setPassengerCapacity(int passengerCapacity) {
        ValidationHelper.checkValueInRange(passengerCapacity,getMinPassengers(),getMaxPassengers(),getErrorPassengersCapacity());
        this.passengerCapacity = passengerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    private void setPricePerKilometer(double pricePerKilometer) {
        ValidationHelper.checkValueInRange(pricePerKilometer,getMinPrice(),getMaxPrice(),getErrorPrice());
        this.pricePerKilometer = pricePerKilometer;
    }

    @Override
    public abstract VehicleType getVehicleType();


    @Override
    public String toString() {
        return String.format("Passenger capacity: %d%n" +
                "Price per kilometer: %.2f%n" +
                "Vehicle type: %s%n", getPassengerCapacity(), getPricePerKilometer(), getVehicleType());
    }

    protected int getMinPassengers() {
        return MIN_PASSENGER_CAPACITY;
    }

    protected int getMaxPassengers() {
        return MAX_PASSENGER_CAPACITY;
    }

    protected String getErrorPassengersCapacity() {
        return ERROR_PASSENGER_CAPACITY;
    }

    protected double getMinPrice() {
        return MIN_PRICE_VALUE;
    }

    protected double getMaxPrice() {
        return MAX_PRICE_VALUE;
    }

    protected String getErrorPrice() {
        return ERROR_PRICE_PER_KILOMETER;
    }


}
