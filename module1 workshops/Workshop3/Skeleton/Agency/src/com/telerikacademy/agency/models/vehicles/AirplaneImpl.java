package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.ValidationHelper;
import com.telerikacademy.agency.models.contracts.Airplane;
import com.telerikacademy.agency.models.enums.VehicleType;

public class AirplaneImpl extends VehicleImpl implements Airplane {

    private boolean hasFreeFood;

    public AirplaneImpl(int passengerCapacity, double pricePerKilometer, boolean hasFreeFood) {
        super(passengerCapacity, pricePerKilometer);
        setHasFreeFood(hasFreeFood);
    }

    @Override
    public boolean hasFreeFood() {
        return hasFreeFood;
    }

    private void setHasFreeFood(boolean hasFreeFood) {
        this.hasFreeFood = hasFreeFood;
    }

    @Override
    public VehicleType getVehicleType() {
        return VehicleType.AIR;
    }

    @Override
    public String toString() {
        return String.format("Airplane ----%n%s" +
                "Has free food: %s%n", super.toString(), hasFreeFood());
    }



}
