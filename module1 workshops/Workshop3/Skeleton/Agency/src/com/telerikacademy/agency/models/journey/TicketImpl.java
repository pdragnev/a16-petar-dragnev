package com.telerikacademy.agency.models.journey;

import com.telerikacademy.agency.models.ValidationHelper;
import com.telerikacademy.agency.models.contracts.Journey;
import com.telerikacademy.agency.models.contracts.Ticket;

public class TicketImpl implements Ticket {
    private static final String JOURNEY_CAN_T_BE_NULL = "Journey can't be null";
    private static final String COSTS_POSITIVE = "Costs should be positive";
    private Journey journey;
    private double administrativeCost;

    public TicketImpl(Journey journey, double administrativeCost) {
        setJourney(journey);
        setAdministrativeCost(administrativeCost);
    }

    @Override
    public Journey getJourney() {
        return journey;
    }

    private void setJourney(Journey journey) {
        ValidationHelper.checkObjectNotNull(journey, JOURNEY_CAN_T_BE_NULL);
        this.journey = journey;
    }

    @Override
    public double getAdministrativeCosts() {
        return administrativeCost;
    }

    private void setAdministrativeCost(double administrativeCost) {
        ValidationHelper.checkValueInRange(administrativeCost,0,Double.MAX_VALUE, COSTS_POSITIVE);
        this.administrativeCost = administrativeCost;
    }

    @Override
    public double calculatePrice() {
        return getAdministrativeCosts() * getJourney().calculateTravelCosts();
    }

    @Override
    public String toString() {
        return String.format("Ticket ----%n" +
                "Destination: %s%n" +
                "Price: %.2f%n", getJourney().getDestination(), calculatePrice());
    }
}
