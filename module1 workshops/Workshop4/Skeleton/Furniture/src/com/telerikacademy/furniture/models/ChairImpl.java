package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.Chair;
import com.telerikacademy.furniture.models.enums.MaterialType;

public class ChairImpl extends FurnitureBase implements Chair {

    private static final int LEGS_MIN = 1;
    private static final String LEGS_ERROR = String.format("Table can't have less than %d leg",LEGS_MIN);
    private int numberOfLegs;

    public ChairImpl(String model, MaterialType materialType, double price, double height, int numberOfLegs) {
        super(model, materialType, price, height);
        setNumberOfLegs(numberOfLegs);
    }

    @Override
    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    private void setNumberOfLegs(int numberOfLegs) {
        ValidationHelper.checkValueInRange(numberOfLegs, LEGS_MIN, Integer.MAX_VALUE, LEGS_ERROR);
        this.numberOfLegs = numberOfLegs;
    }

    @Override
    public String toString() {
        return String.format("%s, Legs: %d", super.toString(), getNumberOfLegs());
    }
}
