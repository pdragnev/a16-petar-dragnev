package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.Furniture;
import com.telerikacademy.furniture.models.enums.MaterialType;

public class FurnitureBase implements Furniture {
    private static final String MODEL_NULL_ERROR = "Model can't be null";
    private static final int MODEL_MIN_LEN = 3;
    private static final String MODEL_LEN_ERROR = String.format("Model can't be less that %d symbols.", MODEL_MIN_LEN);
    private static final String PRICE_ERROR = "Price can't be less or equal 0";
    private static final String HEIGHT_ERROR = "Height can't be less or equal 0";

    private String model;
    private MaterialType materialType;
    private double price;
    private double height;

    FurnitureBase(String model, MaterialType materialType, double price, double height) {
        setModel(model);
        setPrice(price);
        setHeight(height);
        this.materialType = materialType;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public MaterialType getMaterialType() {
        return materialType;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public double getHeight() {
        return height;
    }

    private void setModel(String model) {
        ValidationHelper.checkObjectNull(model, MODEL_NULL_ERROR);
        ValidationHelper.checkStringLength(model, MODEL_MIN_LEN, Integer.MAX_VALUE, MODEL_LEN_ERROR);
        this.model = model;
    }

    private void setPrice(double price) {
        ValidationHelper.checkValueInRangeExcluded(price, 0, Double.MAX_VALUE, PRICE_ERROR);
        this.price = price;
    }

    void setHeight(double height) {
        ValidationHelper.checkValueInRangeExcluded(height, 0, Double.MAX_VALUE, HEIGHT_ERROR);
        this.height = height;
    }

    @Override
    public String toString() {
        return String.format("Type: %s, Model: %s, Material: %s, Price: %.2f, Height: %.2f",
                getFurnitureClassName(), getModel(), getMaterialType(), getPrice(), getHeight());
    }

    private String getFurnitureClassName() {
        return getClass().getSimpleName().replace("Impl", "");

    }
}
