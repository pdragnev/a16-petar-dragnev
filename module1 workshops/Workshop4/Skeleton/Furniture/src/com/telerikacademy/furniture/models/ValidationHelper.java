package com.telerikacademy.furniture.models;

public class ValidationHelper {

    public static void checkValueInRange(double value, double min, double max, String error) {
        if (value < min || value > max) {
            throw new IllegalArgumentException(error);
        }
    }

    public static void checkValueInRangeExcluded(double value, double min, double max, String error) {
        if (value <= min || value >= max) {
            throw new IllegalArgumentException(error);
        }
    }

    public static void checkStringLength(String value, int min, int max, String error) {
        checkValueInRange(value.length(), min, max, error);
    }

    public static void checkObjectNull(Object o, String error) {
        if (o == null) {
            throw new IllegalArgumentException(error);
        }
    }

    public static void checkStringOnlyDigits(String value, String error) {
        if (!value.matches("[0-9]+")) {
            throw new IllegalArgumentException(error);
        }
    }

    public static void checkStringLenEqual(String value, int size) {
        if (value.length() != size) {
            throw new IllegalArgumentException();
        }
    }
}
