package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.Company;
import com.telerikacademy.furniture.models.contracts.Furniture;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class CompanyImpl implements Company {
    private static final String NAME_NULL_ERROR = "Name can't be null";
    private static final int NAME_MIN_LEN = 5;
    private static final String NAME_LEN_ERROR = String.format("Name can't be less than %d symbols", NAME_MIN_LEN);
    private static final int EXACT_REGISTRATION_NUMBER_LENGTH = 10;
    private static final String REGISTRATION_NUMBER_NOT_ALL_DIGITS_ERROR = "There aren't only digits in registrationNumber";
    private static final String REGISTRATION_NUMBER_NULL_ERROR = "RegistationNumber is null";
    private static final String MODEL_NULL_ERROR = "Model is null";
    private static final String REMOVE_ERROR = "Furniture for removing can't be null";
    private static final String ADD_NULL_ERROR = "Furniture to add is null";

    private String name;
    private String registrationNumber;
    private List<Furniture> furnitures;

    public CompanyImpl(String name, String registrationNumber) {
        setName(name);
        setRegistrationNumber(registrationNumber);
        furnitures = new ArrayList<>();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRegistrationNumber() {
        return registrationNumber;
    }

    @Override
    public List<Furniture> getFurnitures() {
        sort();
        return new ArrayList<>(furnitures);
    }

    private void setName(String name) {
        ValidationHelper.checkObjectNull(name, NAME_NULL_ERROR);
        ValidationHelper.checkStringLength(name, NAME_MIN_LEN, Integer.MAX_VALUE, NAME_LEN_ERROR);
        this.name = name;
    }

    private void setRegistrationNumber(String registrationNumber) {
        ValidationHelper.checkObjectNull(registrationNumber, REGISTRATION_NUMBER_NULL_ERROR);
        ValidationHelper.checkStringLenEqual(registrationNumber, EXACT_REGISTRATION_NUMBER_LENGTH);
        ValidationHelper.checkStringOnlyDigits(registrationNumber, REGISTRATION_NUMBER_NOT_ALL_DIGITS_ERROR);
        this.registrationNumber = registrationNumber;
    }

    @Override
    public void add(Furniture furniture) {
        ValidationHelper.checkObjectNull(furniture, ADD_NULL_ERROR);
        furnitures.add(furniture);
    }

    @Override
    public String catalog() {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(String.format("%s - %s - %s %s%n", getName(), getRegistrationNumber(),
                getFurnitures().isEmpty() ? "no" : String.format("%d", getFurnitures().size()),
                getFurnitures().size() == 1 ? "furniture" : "furnitures"));

        for (Furniture furniture : getFurnitures()) {
            strBuilder.append(furniture).append(System.lineSeparator());
        }
        return strBuilder.toString().trim();
    }

    @Override
    public Furniture find(String model) {
        ValidationHelper.checkObjectNull(model, MODEL_NULL_ERROR);
  /*      for (Furniture furniture : furnitures) {
            if (model.equals(furniture.getModel())) {
                return furniture;
            }
        }
        return null;
*/
        return furnitures.stream()
                .filter(f -> f.getModel().equalsIgnoreCase(model))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void remove(Furniture furniture) {
        ValidationHelper.checkObjectNull(furniture, REMOVE_ERROR);
        furnitures.remove(furniture);
    }

 /*   private void furnitureSort(List<Furniture> furnitures) {
        int n = furnitures.size();
        for (int i = 0; i < n - 1; i++)
            for (int j = 0; j < n - i - 1; j++)
                if (compare(furnitures.get(j), furnitures.get(j + 1)) > 0) {
                    Furniture tempFurniture = furnitures.get(j);
                    furnitures.set(j, furnitures.get(j + 1));
                    furnitures.set(j + 1, tempFurniture);
                }
    }

    private int compare(Furniture furniture1, Furniture furniture2) {
        int compareResult = (int) (furniture1.getPrice() - furniture2.getPrice());
        if (compareResult == 0) {
            compareResult = furniture1.getModel().compareTo(furniture2.getModel());
        }
        return compareResult;
    }*/

    private void sort() {
        furnitures.sort(
                Comparator.comparing(Furniture::getPrice)
                        .thenComparing(Furniture::getModel));
    }

}
