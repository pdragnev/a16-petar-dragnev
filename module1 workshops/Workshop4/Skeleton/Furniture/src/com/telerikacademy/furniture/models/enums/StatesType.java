package com.telerikacademy.furniture.models.enums;

public enum StatesType {
    NORMAL,
    CONVERTED;

    @Override
    public String toString() {
        switch (this){
            case NORMAL: return "Normal";
            case CONVERTED: return "Converted";
        }
        throw new IllegalArgumentException();
    }
}
