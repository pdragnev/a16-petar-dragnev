package com.telerikacademy.furniture.models.enums;

public enum ChairType {
    NORMAL,
    ADJUSTABLE,
    CONVERTIBLE;

    @Override
    public String toString() {
        switch (this){
            case NORMAL: return "Normal";
            case ADJUSTABLE: return "Adjustable";
                case CONVERTIBLE: return "Convertible";
        }
        throw new IllegalArgumentException();
    }
}
