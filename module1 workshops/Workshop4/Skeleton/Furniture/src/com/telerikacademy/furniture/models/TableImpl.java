package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.Table;
import com.telerikacademy.furniture.models.enums.MaterialType;

public class TableImpl extends FurnitureBase implements Table {
    private static final String LENGTH_ERROR = "Length can't be less or equal 0";
    private static final String WIDTH_ERROR = "Width can't be less or equal 0";

    private double length;
    private double width;

    public TableImpl(String model, MaterialType materialType, double price, double height, double length, double width) {
        super(model, materialType, price, height);
        setLength(length);
        setWidth(width);
    }

    @Override
    public double getLength() {
        return length;
    }

    @Override
    public double getWidth() {
        return width;
    }

    @Override
    public double getArea() {
        return length * width;
    }

    private void setLength(double length) {
        ValidationHelper.checkValueInRangeExcluded(length, 0, Double.MAX_VALUE, LENGTH_ERROR);
        this.length = length;
    }

    private void setWidth(double width) {
        ValidationHelper.checkValueInRangeExcluded(width, 0, Double.MAX_VALUE, WIDTH_ERROR);
        this.width = width;
    }

    @Override
    public String toString() {
        return String.format("%s, Length: %.2f, Width: %.2f, Area: %.4f",
                super.toString(), getLength(), getWidth(), getArea());
    }
}
