package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.ConvertibleChair;
import com.telerikacademy.furniture.models.enums.MaterialType;
import com.telerikacademy.furniture.models.enums.StatesType;

public class ConvertibleChairImpl extends ChairImpl implements ConvertibleChair {

    private static final double CONVERTED_HEIGHT = 0.1;
    private StatesType statesType;
    private double normalHeight;

    public ConvertibleChairImpl(String model, MaterialType materialType, double price, double height, int numberOfLegs) {
        super(model, materialType, price, height, numberOfLegs);
        statesType = StatesType.NORMAL;
        normalHeight = getHeight();
    }

    @Override
    public boolean getConverted() {
        return statesType == StatesType.CONVERTED;
    }

    @Override
    public void convert() {
        if (statesType == StatesType.NORMAL) {
            statesType = StatesType.CONVERTED;
            setHeight(CONVERTED_HEIGHT);
        } else {
            statesType = StatesType.NORMAL;
            setHeight(normalHeight);
        }
    }

    @Override
    public String toString() {
        return String.format("%s, State: %s",super.toString(),getConverted() ? StatesType.CONVERTED : StatesType.NORMAL);
    }
}
