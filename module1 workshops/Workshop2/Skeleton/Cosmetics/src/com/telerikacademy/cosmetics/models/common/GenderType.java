package com.telerikacademy.cosmetics.models.common;

public enum GenderType {
    MEN,
    WOMEN,
    UNISEX;

    @Override
    public String toString() {
        switch (this) {
            case MEN:
                return "Men";
            case WOMEN:
                return "Woman";
            case UNISEX:
                return "Unisex";
        }
        throw new IllegalArgumentException();
    }
}

