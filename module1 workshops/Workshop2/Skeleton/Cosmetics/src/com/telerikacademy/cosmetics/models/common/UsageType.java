package com.telerikacademy.cosmetics.models.common;

public enum UsageType {
    EVERY_DAY,
    MEDICAL,
    EVERYDAY;

    @Override
    public String toString() {
        switch (this) {
            case EVERY_DAY:
                return "Every day";
            case EVERYDAY:
                return "Everyday";
            case MEDICAL:
                return "Medical";
            default:
                throw new IllegalArgumentException();
        }
    }
}
