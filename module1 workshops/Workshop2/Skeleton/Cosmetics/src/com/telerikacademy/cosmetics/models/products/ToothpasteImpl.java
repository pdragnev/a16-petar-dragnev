package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.ValidationHelper;
import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Toothpaste;

import java.util.ArrayList;
import java.util.List;

public class ToothpasteImpl extends ProductImpl implements Toothpaste {
    private List<String> ingredients;

    public ToothpasteImpl(String name, String brand, double price, GenderType gender, List<String> ingredients) {
        super(name, brand, price, gender);
        setIngredients(ingredients);
    }

    public List<String> getIngredients() {
        return new ArrayList<>(ingredients);
    }

    private void setIngredients(List<String> ingredients) {
        ValidationHelper.CheckIngredientsNull(ingredients);
        this.ingredients = ingredients;
    }

    @Override
    public String print() {
        return toString();
    }

    @Override
    public String toString() {
        return String.format("%s #Ingredients: %s" + System.lineSeparator(), super.toString(), getIngredients().toString());
    }
}
