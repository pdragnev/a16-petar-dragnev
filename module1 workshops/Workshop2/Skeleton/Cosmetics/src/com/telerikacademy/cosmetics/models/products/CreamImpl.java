package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.ValidationHelper;
import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.ScentType;
import com.telerikacademy.cosmetics.models.contracts.Cream;
import com.telerikacademy.cosmetics.models.contracts.Product;


public class CreamImpl extends ProductImpl implements Cream {
    private static final int MIN_NAME_LENGTH = 3;
    private static final int MAX_NAME_LENGTH = 15;
    private static final int MIN_BRAND_LENGTH = 3;
    private static final int MAX_BRAND_LENGTH = 15;
    private ScentType scent;


    public CreamImpl(String name, String brand, double price, GenderType gender, ScentType scent) {
        super(name, brand, price, gender);
        setScentType(scent);
    }

    @Override
    protected int getMaxNameLength() {
        return MAX_NAME_LENGTH;
    }

    @Override
    protected int getMinNameLength() {
        return MIN_NAME_LENGTH;
    }

    @Override
    protected int getMaxBrandLength() {
        return MAX_BRAND_LENGTH;
    }

    @Override
    protected int getMinBrandLength() {
        return MIN_BRAND_LENGTH;
    }

    @Override
    public ScentType getScent() {
        return scent;
    }

    private void setScentType(ScentType scent) {
        this.scent = scent;
    }

    @Override
    public String print() {
        return toString();
    }

    @Override
    public String toString() {
        return String.format("%s #Scent: %s" + System.lineSeparator(),super.toString(), getScent());
    }
}
