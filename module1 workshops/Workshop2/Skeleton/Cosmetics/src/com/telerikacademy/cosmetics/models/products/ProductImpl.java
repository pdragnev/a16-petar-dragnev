package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.ValidationHelper;
import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;

public class ProductImpl implements Product {
    private static final int MIN_NAME_LENGTH = 3;
    private static final int MAX_NAME_LENGTH = 10;
    private static final int MIN_BRAND_LENGTH = 2;
    private static final int MAX_BRAND_LENGTH = 10;
    private String name;
    private String brand;
    private double price;
    private GenderType gender;

    public ProductImpl(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        setGender(gender);
    }

    @Override
    public String getName() {
        return name;
    }

    private void setName(String name) {
        ValidationHelper.CheckStringNull(name);
        ValidationHelper.CheckStrLen(name, getMinNameLength(), getMaxNameLength());
        this.name = name;
    }

    protected int getMaxNameLength() {
        return MAX_NAME_LENGTH;
    }

    protected int getMinNameLength() {
        return MIN_NAME_LENGTH;
    }

    @Override
    public String getBrand() {
        return brand;
    }

    private void setBrand(String brand) {
        ValidationHelper.CheckStringNull(brand);
        ValidationHelper.CheckStrLen(brand, getMinBrandLength(), getMaxBrandLength());
        this.brand = brand;
    }

    protected int getMaxBrandLength() {
        return MAX_BRAND_LENGTH;
    }

    protected int getMinBrandLength() {
        return MIN_BRAND_LENGTH;
    }

    @Override
    public double getPrice() {
        return price;
    }

    private void setPrice(double price) {
        ValidationHelper.CheckPrice(price);
        this.price = price;
    }

    @Override
    public GenderType getGender() {
        return gender;
    }

    private void setGender(GenderType gender) {
        this.gender = gender;
    }

    @Override
    public String print() {
        return toString();
    }

    @Override
    public String toString() {
        return String.format("#%s %s" + System.lineSeparator() +
                        " #Price: $%.2f" + System.lineSeparator() +
                        " #Gender: %s" + System.lineSeparator()
                , getName(), getBrand(), getPrice(), getGender());
    }
}
