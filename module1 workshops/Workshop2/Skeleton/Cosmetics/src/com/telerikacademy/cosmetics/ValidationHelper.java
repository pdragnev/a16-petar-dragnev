package com.telerikacademy.cosmetics;


import com.telerikacademy.cosmetics.models.contracts.Product;

import java.util.List;

public class ValidationHelper {

    private static final String LENGTH_ERROR_MSG_FORMAT = "The length should be between %d and %d characters!";
    private static final String PRICE_ERROR_MSG = "Price can't be negative!";
    private static final String MILLILITERS_ERROR_MSG = "Milliliters can't be negative!";
    private static final String PRODUCT_IS_NULL_MSG = "Product is null";
    private static final String STRING_IS_NULL_MSG = "String is null";

    public static void CheckStrLen(String str, int minValue, int maxValue) {
        String msg = String.format(LENGTH_ERROR_MSG_FORMAT, minValue, maxValue);
        if (str.length() < minValue || str.length() > maxValue) {
            throw new IllegalArgumentException(msg);
        }
    }

    public static void CheckPrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException(PRICE_ERROR_MSG);
        }
    }

    public static void CheckMilliliters(int milliliters) {
        if (milliliters < 0) {
            throw new IllegalArgumentException(MILLILITERS_ERROR_MSG);
        }
    }

    public static void CheckProductNull(Product product) {
        if (product == null) {
            throw new IllegalArgumentException(PRODUCT_IS_NULL_MSG);
        }
    }

    public static void CheckStringNull (String str){
        if (str==null){
            throw new IllegalArgumentException(STRING_IS_NULL_MSG);
        }
    }

    public static void CheckIngredientsNull(List<String> ingredients) {
        if (ingredients == null){
            throw new IllegalArgumentException();
        }
    }

 /*   public static  void CheckObjectNull (Object o){
        if (o == null){
            throw new IllegalArgumentException();
        }
    }*/

}
