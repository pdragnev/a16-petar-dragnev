package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.ValidationHelper;
import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;


public class ShampooImpl extends ProductImpl implements Shampoo {
    private int milliliters;
    private UsageType usage;

    public ShampooImpl(String name, String brand, double price, GenderType gender, int milliliters, UsageType usage) {
        super(name, brand, price, gender);
        setMilliliters(milliliters);
        setUsage(usage);
    }

    private void setMilliliters(int milliliters) {
        ValidationHelper.CheckMilliliters(milliliters);
        this.milliliters = milliliters;
    }

    private void setUsage(UsageType usage) {
        this.usage = usage;
    }

    public int getMilliliters() {
        return milliliters;
    }

    public UsageType getUsage() {
        return usage;
    }


    @Override
    public String print() {
        return toString();
    }

    @Override
    public String toString() {
        return String.format("%s #Milliliters: %d" + System.lineSeparator() +
                        " #Usage: %s" + System.lineSeparator(), super.toString(),getMilliliters(), getUsage());
    }
}
