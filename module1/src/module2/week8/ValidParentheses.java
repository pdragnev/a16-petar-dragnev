package module2.week8;

import java.util.Stack;

public class ValidParentheses {
    public static void main(String[] args) {


        Character[] characters1 = {'(', ')', '{', '}', '[', ']'};
        Character[] characters2 = {'(', '{', '}', '[', ']', ')'};
        char[] characters3 = {'(', '{', '[', ']', '}', ')'};
        String s = "";
        Stack<Character> stack = new Stack<>();
        char[] characters = s.toCharArray();

        for (char character : characters) {

            if (stack.size() < 1) {
                stack.push(character);
                continue;
            }
            Character temp = stack.peek();
            stack.push(character);
            if (temp.equals('(') && stack.peek().equals(')')) {
                stack.pop();
                stack.pop();
            } else if (temp.equals('{') && stack.peek().equals('}')) {
                stack.pop();
                stack.pop();
            } else if (temp.equals('[') && stack.peek().equals(']')) {
                stack.pop();
                stack.pop();
            }
        }

        System.out.println(stack.isEmpty());
    }
}