package module1.Week1_2.Classes1;

/*public class Main {

    public static void main(String[] args) {
        String[] names = {"Pesho", "Gosho", "Ivana", "Misho", "Rumi"};
        int[] ages = {23, 30, 21, 25, 27};
        String[] phones = {"0887123456", "0887654321", "0887112233", "0888888888", "0885588111"};

        String[][] interests = {
                {"Football", "Computer Games"},
                {"Ski", "Climbing", "Programming"},
                {"Books", "Cooking"},
                {"Music", "Play drums", "Yoga"},
                {"Programming", "Dancing"}
        };

        // Advanced Task: implement friends
        int[][] friends = {
                {1, 3},
                {4},
                {0, 4},
                {0, 1, 2, 4},
                {2}
        };
        // Advanced Task

        for (int p = 0; p < names.length; p++) {
            introducePerson(names[p], ages[p]);
            sharePhone(phones[p]);
            showFriends(names, friends, p);

            System.out.println();
            System.out.println();
        }
    }

    public static void introducePerson(String name, int age) {
        System.out.printf("Hey, I am %s and I am %d years old :)\n", name, age);
    }

    public static void sharePhone(String phone) {
        System.out.printf("You can contact me at %s\n", phone);
    }

    public static void showFriends(String[] names, int[][] friends, int index) {
        System.out.print("My friends are: ");

        for (int f = 0; f < friends[index].length; f++) {
            System.out.printf("%s ", names[friends[index][f]]);
        }
    }
}*/


import java.util.ArrayList;
import java.util.Arrays;

public class Person {
    private String name;
    private int age;
    private String phone;
    private ArrayList<String> interests;
    private ArrayList<Person> friends;

    public Person(String name, int age, String phone, String... interests) {
        this.name = name;
        this.age = age;
        this.phone = phone;
        this.interests = new ArrayList<>(Arrays.asList(interests));
        this.friends = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getPhone() {
        return phone;
    }

    public ArrayList<String> getInterests() {
        return interests;
    }

    public ArrayList<Person> getFriends() {
        return friends;
    }

    public void intoducePerson() {
        System.out.printf("Hey, I am %s and I am %d years old. ", this.name, this.age);
    }

    public void sharePhone() {
        System.out.printf("You can contact me at %s. ", this.phone);
    }

    public void addFriend(Person friend){
        this.friends.add(friend);
    }

    public void showFriends() {
        if (this.friends.size() == 0) {
            System.out.printf("I have no friends :)", this.name);
            return;
        }
        System.out.printf("My friend%s %s: ", this.friends.size() == 1 ? "" : "s",this.friends.size() == 1 ? "is" : "are");
        for (Person friend : this.friends) {
            System.out.printf("%s ", friend.getName());

        }
    }
}




