package module1.Week1_2.Classes1;

public class Point {
    private double x = -1;
    private double y = -1;


    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point(double x) {
        this(x,-1);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void translatePoint (double next_x, double next_y){
        this.x +=next_x;
        this.y +=next_y;
    }

    public void getPoint(){
        System.out.printf("Point(%.2f,%.2f)",this.x,this.y);
    }
}
