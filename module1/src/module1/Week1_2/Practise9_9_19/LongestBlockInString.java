package module1.Week1_2.Practise9_9_19;
import java.io.ByteArrayInputStream;
import java.util.Scanner;

public class LongestBlockInString {
    public static void testInput() {
        String input = "abc";
        System.setIn(new ByteArrayInputStream(input.getBytes()));
    }

    public static void main(String[] args) {
        //testInput();
        Scanner scanner = new Scanner(System.in);
        String[] inputSymbols = scanner.nextLine().split("");
        String output = inputSymbols[0];
        String currentOutput = inputSymbols[0];
        String output1 = inputSymbols[0];
        for (int i = 1; i < inputSymbols.length; i++) {
            if (output.equals(inputSymbols[i])){
                currentOutput+=inputSymbols[i];
                if (i+1<inputSymbols.length){
                continue;}
            }
            if (currentOutput.length()>output1.length()){
                output1=currentOutput;
            }
            output=inputSymbols[i];
            currentOutput=inputSymbols[i];
            if (currentOutput.length()>output1.length()){
                output1=currentOutput;
            }
        }
        System.out.println(output1);
    }
}
