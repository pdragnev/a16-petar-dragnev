package module1.Week1_2.Practise9_9_19;

import java.util.Scanner;

public class KnightMoves {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[][] matrix = new int[n][n];

        // All possible horse moves (8) ordered by priority (top most, left most)
        int[] horseMovesHorizontal = {-2, -2, -1, -1, +1, +1, +2, +2};
        int[] horseMovesVertical = {-1, +1, -2, +2, -2, +2, -1, +1};

        int movesCounter = 1;
        // For each cell in the matrix that is not used try to make horse moves
        for (int r = 0; r < n; r++) {
            for (int c = 0; c < n; c++) {
                int currentRow = r;
                int currentCol = c;

                // Make moves while cell is not visited
                while (matrix[currentRow][currentCol] == 0) {
                    matrix[currentRow][currentCol] = movesCounter;
                    movesCounter++;

                    // Search for possible move
                    for (int move = 0; move < horseMovesHorizontal.length; move++) {
                        int nextRow = currentRow + horseMovesHorizontal[move];
                        int nextCol = currentCol + horseMovesVertical[move];

                        // Check if move goes out of the matrix
                        if (nextRow < 0 || nextRow >= matrix.length ||
                                nextCol < 0 || nextCol >= matrix.length) {
                            continue;
                        }

                        // Check if the cell is visited
                        if (matrix[nextRow][nextCol] != 0) {
                            continue;
                        }

                        currentRow = nextRow;
                        currentCol = nextCol;
                        break;
                    }
                }
            }
        }

        // Print the output
        for (int[] row : matrix) {
            String result = "";
            for (int cell : row) {
                result += cell + " ";
            }
            System.out.println(result.trim());
        }
    }
}
/*
package module1.Week1_2.Practise9_9_19;


import java.io.ByteArrayInputStream;
import java.util.Scanner;

public class KnightMoves {
    static boolean isDone=false;

    static boolean isSafe(int x, int y, int[][] sol) {
        return (x >= 0 && x < sol.length && y >= 0 &&
                y < sol.length && sol[x][y] == -1);
    }

    static void printSolution(int sol[][]) {
        for (int x = 0; x < sol.length; x++) {
            for (int y = 0; y < sol.length; y++)
                System.out.printf("%d ", sol[x][y]);
            System.out.println();
        }
    }

    static boolean solveKT(int size) {
        int[][] sol = new int[size][size];

        */
/* Initialization of solution matrix *//*

        for (int x = 0; x < size; x++)
            for (int y = 0; y < size; y++)
                sol[x][y] = -1;

       */
/* xMove[] and yMove[] define next move of Knight.
          xMove[] is for next value of x coordinate
          yMove[] is for next value of y coordinate *//*

        int[] xMove = {-1, -2, -2, 1, -1, 1, 2, 2};
        int[] yMove = {-2, -1, 1, -2, 2, 2, -1, 1};

        sol[0][0] = 1;

        solveKTUtil(0, 0, 2, sol, xMove, yMove);

        int maxNumber = 1;
        for (int i = 0; i < sol.length; i++) {
            for (int j = 0; j < sol.length; j++) {
                if (sol[i][j] > maxNumber) {
                    maxNumber = sol[i][j];
                }
            }
        }
        for (int i = 0; i < sol.length; i++) {
            for (int j = 0; j < sol.length; j++) {
                if (sol[i][j] == -1) {
                    sol[i][j] = ++maxNumber;
                }
            }
        }


        printSolution(sol);

        return true;
    }

    static boolean solveKTUtil(int x, int y, int movei,
                               int[][] sol, int[] xMove,
                               int[] yMove) {
        int next_x, next_y;
        if (movei >= sol.length * sol.length)
            return true;

        for (int k = 0; k < 8; k++) {
            next_x = x + xMove[k];
            next_y = y + yMove[k];
            if (isSafe(next_x, next_y, sol) && !isDone) {
                sol[next_x][next_y] = movei;
                if (solveKTUtil(next_x, next_y, movei + 1,
                        sol, xMove, yMove))
                    return true;
            }
        }
        isDone=true;
        return false;
    }


    public static void testInput() {
        String input = "4";
        System.setIn(new ByteArrayInputStream(input.getBytes()));
    }

    public static void main(String args[]) {
        testInput();
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        solveKT(size);
    }
}
*/
