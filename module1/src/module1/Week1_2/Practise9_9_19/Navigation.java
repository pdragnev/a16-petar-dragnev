package module1.Week1_2.Practise9_9_19;

import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Scanner;

public class Navigation {
    public static void testInput() {
        String input = "5 \n" +
                "6\n" +
                "4\n" +
                "14 27 1 5";
        System.setIn(new ByteArrayInputStream(input.getBytes()));
    }
    public static void main(String[] args) {
        testInput();
        Scanner scanner = new Scanner(System.in);
        int rowLenght = scanner.nextInt();
        int colLenght = scanner.nextInt();
        int moves = scanner.nextInt();
        scanner.nextLine();
        String instructions = scanner.nextLine();

        BigInteger [][] matrix = new BigInteger[rowLenght][colLenght];
        BigInteger currentCell = new BigInteger("1");
        for (int i = matrix.length-1; i >= 0; i--) {
            for (int j = 0; j < matrix[i].length; j++) {
                    matrix[i][j]=currentCell;
                    currentCell=currentCell.multiply(new BigInteger("2"));
            }
            currentCell = matrix[i][0].multiply(new BigInteger("2"));
        }
        for (BigInteger[] arr:matrix){
            System.out.println(Arrays.toString(arr));
        }
    }



}
