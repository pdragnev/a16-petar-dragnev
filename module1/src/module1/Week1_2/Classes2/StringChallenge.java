package module1.Week1_2.Classes2;

public class StringChallenge {
    public static void main(String[] args) {
         final int MAGIC_NUMBER=2;
        StringBuilder input = new StringBuilder("git is a <<version control>> system for tracking changes in <<computer files>> and coordinating work on those files among multiple people");
        while (input.toString().contains("<<") && input.toString().contains(">> ")){
            input.replace(input.indexOf("<<"),input.indexOf(">>")+MAGIC_NUMBER, input.substring(input.indexOf("<<")+MAGIC_NUMBER, input.indexOf(">>")).toUpperCase());
        }
        System.out.println(input);
    }
}
