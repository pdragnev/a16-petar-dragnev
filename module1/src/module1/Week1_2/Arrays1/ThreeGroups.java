package module1.Week1_2.Arrays1;

import java.util.Arrays;
import java.util.Scanner;

public class ThreeGroups {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String[] stringInput = input.split("\\s");
        int[] intElements = new int[stringInput.length];
        for (int j = 0; j < intElements.length; j++) {
            intElements[j] = Integer.parseInt(stringInput[j]);
        }

        int arr[][] = new int[3][];

        int firstRow = 0,secondRow = 0,thirdRow = 0;
        int a=0, b=0,c=0;
        for (int i = 0; i < intElements.length; i++) {
                switch (intElements[i]%3){
                    case 0:firstRow++; break;
                    case 1:secondRow++; break;
                    case 2:thirdRow++; break;
                }
        }
        arr[0] = new int[firstRow];
        arr[1] = new int[secondRow];
        arr[2] = new int[thirdRow];

        for (int i = 0; i < intElements.length; i++) {
            switch (intElements[i]%3){
                case 0: arr[0][a++] = intElements[i]; break;
                case 1: arr[1][b++] = intElements[i]; break;
                case 2: arr[2][c++] = intElements[i]; break;
            }
        }

        for (int i = 0; i < arr.length; i++) {
            System.out.println(Arrays.toString(arr[i]).
                    replace("[","").replace("]","")
                    .replace(",",""));

        }
    }
}
