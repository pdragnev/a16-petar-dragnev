package module1.Week1_2.Arrays1;

import java.util.Arrays;
import java.util.Scanner;

public class BigNumbers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String sizes = scanner.nextLine();
        String array1 = scanner.nextLine();
        String array2 = scanner.nextLine();
        String[] array1Elements = array1.split("\\s");
        String[] array2Elements = array2.split("\\s");
        int biggerLenght = array1Elements.length > array2Elements.length ? array1Elements.length : array2Elements.length;
        int[] sumElements = new int[biggerLenght];

        int rest = 0;
        for (int i = 0; i < biggerLenght; i++) {
            int numberToSum1 = 0;
            int numberToSum2 = 0;
            if (i < array1Elements.length) {
                numberToSum1 = Integer.parseInt(array1Elements[i]);
            }
            if (i < array2Elements.length) {
                numberToSum2 = Integer.parseInt(array2Elements[i]);
            }
            int sum = numberToSum1 + numberToSum2 + rest;
            rest = 0;
            if (sum > 9) {
                sum -= 10;
                rest = 1;
            }

            sumElements[i] = sum;
        }

        System.out.println(Arrays.toString(sumElements).replace(",","").
                replace("[","").replace("]",""));
    }

}
