package module1.Week1_2.Arrays1;

import java.util.Arrays;
import java.util.Scanner;

public class ArrayReverse {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        String [] lineElements = line.split("\\s");
        String [] reversedElements = new String[lineElements.length];

        for (int i=0; i<lineElements.length; i++){
            reversedElements[lineElements.length-i-1] = lineElements[i];
        }

        System.out.println(Arrays.toString(reversedElements)
                .replace("[","").replace("]",""));



    }
}
