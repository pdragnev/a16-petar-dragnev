package module1.Week1_2.Arrays1;

import java.util.Scanner;

public class Bounce {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int row = scanner.nextInt();
        int column = scanner.nextInt();
        scanner.nextLine();
        long[][] matrix = new long[row][column];

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                if (i == 0 && j == 0) {
                    matrix[i][j] = 1;
                } else if (j == 0) {
                    matrix[i][j] = matrix[i - 1][j] * 2;
                } else {
                    matrix[i][j] = matrix[i][j - 1] * 2;
                }
            }
        }
        long sum = 0;
        int rowMatrix = 0;
        int columnMatrix = 0;

        if (row == 1 || column == 1) {
            sum = 1;
        } else {
            //boolean goingDown = true;
            // boolean goingRight = true;
            boolean firstTimeFlag = true;
            int rowMoving = 1;
            int colMoving = 1;
            while (true) {
                sum += matrix[rowMatrix][columnMatrix];

                if (!firstTimeFlag && (rowMatrix == row - 1 || rowMatrix == 0) && (columnMatrix == column - 1 || columnMatrix == 0)) {
                    break;
                }
                firstTimeFlag = false;

                rowMatrix += rowMoving;
                columnMatrix += colMoving;

                if (rowMatrix == row - 1) {
                    rowMoving = -1;
                } else if (rowMatrix == 0) {
                    rowMoving = 1;
                }

                if (columnMatrix == column - 1) {
                    colMoving = -1;
                } else if (columnMatrix == 0) {
                    colMoving = 1;
                }
            }
        }
        System.out.println(sum);
    }
}


