package module1.Week1_2.Arrays1;

import java.util.Scanner;

public class AboveMainDiagonal2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();
        scanner.nextLine();
        long [][] matrix = new long[input][input];

        for (int i = 0; i < input; i++) {
            for (int j = 0; j < input; j++) {
                if (i == 0 && j == 0) {
                    matrix[i][j] = 1;
                } else if (j == 0) {
                    matrix[i][j] = matrix[i - 1][j] * 2;
                } else {
                    matrix[i][j] = matrix[i][j - 1] * 2;
                }
            }
        }
        long sum = 0;
        for (int i = 0; i < input; i++) {
            for (int j = i; j < input; j++) {
                sum += matrix[i][j];
            }
        }

        System.out.println(sum);

    }
}
