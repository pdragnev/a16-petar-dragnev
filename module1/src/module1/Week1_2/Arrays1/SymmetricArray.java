package module1.Week1_2.Arrays1;
        /*Input
        Read from the standard input
        On the first line, read the number N
        The number of arrays to follow
        On the N lines, read the elements of each array
        Separated by a white space
        Output
        Print to the standard output
        For each of the arrays, print "Yes" or "No"*/

import java.util.Scanner;

public class SymmetricArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberOfRows = scanner.nextInt();
        scanner.nextLine();
        boolean[] foo = new boolean[numberOfRows];
        int k = 0;
        for (int i = 0; i < numberOfRows; i++) {
            boolean flag = true;
            String input = scanner.nextLine();
            String[] stringInput = input.split("\\s");
            int[] intElements = new int[stringInput.length];
            for (int j = 0; j < intElements.length; j++) {
                intElements[j] = Integer.parseInt(stringInput[j]);
            }
            for (int j = 0; j < intElements.length / 2; j++) {
                if (intElements[j] != intElements[intElements.length - j - 1]) {
                    flag = false;
                    break;
                }
            }
            foo[k++] = flag;
        }
        for (int i = 0; i < foo.length; i++) {
            if (foo[i]) {
                System.out.println("Yes");
            } else {
                System.out.println("No");
            }

        }
    }
}
