package module1.Week1_2.Methods;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

public class LongestIncreasingSequence {
    public static void testInput() {
        String input = "8\n" +
                "7\n" +
                "3\n" +
                "2\n" +
                "3\n" +
                "5\n" +
                "2\n" +
                "2\n" +
                "4";
        System.setIn(new ByteArrayInputStream(input.getBytes()));
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        testInput();
        int n = scanner.nextInt();
        scanner.nextLine();

        int[] inputArr = new int[n];
        for (int i = 0; i < n; i++) {
            int input = scanner.nextInt();
            inputArr[i] = input;
        }

        int sequence = 1;
        int counter = 1;


        for (int i = 1; i < inputArr.length; i++) {
            if (inputArr[i - 1] < inputArr[i]) {
                ++counter;
                if (counter > sequence) {
                    sequence = counter;
                }
            } else {
                counter = 1;
            }
        }


        System.out.println(sequence);


    }
}
