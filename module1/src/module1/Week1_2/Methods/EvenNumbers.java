package module1.Week1_2.Methods;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

public class EvenNumbers {
    public static void testInput() {
        String input = "No numbers.";
        System.setIn(new ByteArrayInputStream(input.getBytes()));
    }

    public static boolean isInt(String strNum) {
        try {
            int n = Integer.parseInt(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }
    public static boolean isInt(char strNum) {
        try {
            int n = Integer.parseInt(Character.toString(strNum));
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        //testInput();
        int k=0;
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String[] inputArray = new String[input.length()];
        String foundNumber="";
        for (int i = 0; i < input.length(); i++) {
            if (isInt(input.charAt(i)) && input.length()==1){
                foundNumber = foundNumber+input.charAt(i);
                inputArray[k++]=foundNumber;
            }
            else if (isInt(input.charAt(i))){
                foundNumber = foundNumber+input.charAt(i);
            }
            else if (!foundNumber.isEmpty()){
                inputArray[k++]=foundNumber;
                foundNumber="";
            }
        }
        int maxEvenNumber = -1;
        boolean firstTime = true;

        for (int i = 0; i < inputArray.length; i++) {
            if (isInt(inputArray[i]) && Integer.parseInt(inputArray[i]) % 2 == 0) {
                if (firstTime) {
                    maxEvenNumber = Integer.parseInt(inputArray[i]);
                    firstTime = false;
                } else if (Integer.parseInt(inputArray[i]) > maxEvenNumber) {
                    maxEvenNumber = Integer.parseInt(inputArray[i]);
                }
            }
        }

        System.out.println(maxEvenNumber);
    }
}
