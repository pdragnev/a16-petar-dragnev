package module1.Week1_2.Methods;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class MatrixMaxSum {
    public static void testInput() {
        String input = "6\n" +
                "1 2 3 4 5 6\n" +
                "2 3 4 5 6 7\n" +
                "6 5 4 3 2 1\n" +
                "3 4 5 6 7 8\n" +
                "4 5 6 7 8 9\n" +
                "9 8 7 6 5 4\n" +
                "3 5 3 -5 -4 -2";
        System.setIn(new ByteArrayInputStream(input.getBytes()));
    }

    public static void main(String[] args) {
        testInput();
        Scanner scanner = new Scanner(System.in);
        int row = scanner.nextInt();
        scanner.nextLine();
        int rowLine = 0;
        int[][] inputMatrixInt = new int[row][];
        for (int i = 0; i < row; i++) {
            String lineInput = scanner.nextLine();
            String[] lineInputArray = lineInput.split("\\s");
            inputMatrixInt[rowLine] = new int[lineInputArray.length];
            for (int j = 0; j < lineInputArray.length; j++) {
                inputMatrixInt[rowLine][j] = Integer.parseInt(lineInputArray[j]);
            }
            rowLine++;
        }

        String lineInput = scanner.nextLine();
        String[] lineInputArray = lineInput.split("\\s");
        int [] operationsArray = new int [lineInputArray.length];
        for (int i = 0; i <lineInputArray.length ; i++) {
            operationsArray[i] = Integer.parseInt(lineInputArray[i]);
        }
        ArrayList<Integer> sum = new ArrayList <>();
        for (int i = 0; i < operationsArray.length; i+=2) {
            int sumSelected=0;
            int rowSelected = operationsArray[i];
            int colSelected = operationsArray[i+1];
            if (rowSelected>0){
                for (int j = 0; j < row; j++) {
                    if (colSelected>0){
                        if (j<rowSelected-1){
                            sumSelected+=inputMatrixInt[j][colSelected-1];
                        }
                        else {
                            for (int k = 0; k < colSelected; k++) {
                                sumSelected+=inputMatrixInt[j][k];
                            }
                            break;
                        }
                    }
                    else{
                        if (j>rowSelected-1){
                            sumSelected+=inputMatrixInt[j][Math.abs(colSelected)-1];
                        }
                        else if (j==rowSelected-1){
                            for (int k = 0  ; k < Math.abs(colSelected); k++) {
                                sumSelected+=inputMatrixInt[j][k];
                            }
                        }
                    }
                }
            }
            else{
                for (int j = 0; j < row; j++) {
                    if (colSelected>0){
                        if (j<Math.abs(rowSelected)-1){
                            sumSelected+=inputMatrixInt[j][colSelected-1];
                        }
                        else  if (j==Math.abs(rowSelected)-1) {
                            for (int k = inputMatrixInt[j].length-1; k >= colSelected-1; k--) {
                                sumSelected+=inputMatrixInt[j][k];
                            }
                        }
                    }
                    else{
                        if (j>Math.abs(rowSelected)-1){
                            sumSelected+=inputMatrixInt[j][Math.abs(colSelected)-1];
                        }
                        else  if (j==Math.abs(rowSelected)-1) {
                            for (int k = inputMatrixInt[j].length-1; k >= Math.abs(colSelected)-1; k--) {
                                sumSelected+=inputMatrixInt[j][k];
                            }
                        }
                    }
                }
            }
            sum.add(sumSelected);
        }
        Collections.sort(sum);
        int outputValue = sum.get(sum.size()-1);
        System.out.println(outputValue);

    }
}
