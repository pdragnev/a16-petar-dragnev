package module1.Week1_2.Methods;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

public class PrimesToN {
    public static void testInput() {
        String input = "24";
        System.setIn(new ByteArrayInputStream(input.getBytes()));
    }

    public static void main(String[] args) {
        //testInput();
        Scanner scanner = new Scanner(System.in);

        String asd = "asd";
        int n = scanner.nextInt();
        for (int i = 1; i <= n; i++) {
            boolean isPrime = true;
            for (int j = 2; j < i - 1; j++) {
                if (i % j == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) {
                System.out.printf("%d ", i);
            }
        }
    }
}

