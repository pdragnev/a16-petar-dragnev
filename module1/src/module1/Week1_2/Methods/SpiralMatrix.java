package module1.Week1_2.Methods;

import java.util.Arrays;
import java.util.Scanner;

public class SpiralMatrix {

    public static void main(String[] args) {
        TestInput.testInput("3");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int k = 1;
        int[][] matrix = new int[n][n];


        int rowMatrix = 0;
        int columnMatrix = 0;


        int rowMoving = 0;
        int colMoving = 1;
        int bottom = n - 1;
        int right = n - 1;
        int top = 0;
        int left = 0;
        int dir = 0;
        while (true) {
            if (k - 1 == Math.pow(n, 2)) {
                break;
            }
            matrix[rowMatrix][columnMatrix] = k++;


            rowMatrix += rowMoving;
            columnMatrix += colMoving;

            if (rowMatrix == top && columnMatrix == right && dir == 0) {
                rowMoving = 1;
                colMoving = 0;
                top++;
                dir = 1;
            }
            if (rowMatrix == bottom && columnMatrix == right && dir == 1) {
                rowMoving = 0;
                colMoving = -1;
                right--;
                dir = 2;
            }
            if (rowMatrix == bottom && columnMatrix == left && dir == 2) {
                rowMoving = -1;
                colMoving = 0;
                bottom--;
                dir = 3;
            }
            if (rowMatrix == top && columnMatrix == left && dir == 3) {
                rowMoving = 0;
                colMoving = 1;
                left++;
                dir = 0;
            }


        }

        for (int[] arr : matrix) {
            System.out.println(Arrays.toString(arr)
                    .replace("[", "")
                    .replace("]","")
                    .replace(",", ""));
        }
    }
}

