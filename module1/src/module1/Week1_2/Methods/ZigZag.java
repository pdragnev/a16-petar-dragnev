package module1.Week1_2.Methods;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

public class ZigZag {
    public static void testInput() {
        String input = "5 6";
        System.setIn(new ByteArrayInputStream(input.getBytes()));
    }
    public static void main(String[] args) {
        testInput();
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String [] inputArray = input.split("\\s");
        int row = Integer.parseInt(inputArray[0]);
        int column = Integer.parseInt(inputArray[1]);
        //long[][] matrix = new long[row][column];

        //long currentNumber = 1;

      /*  for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                matrix[i][j] = currentNumber;
                currentNumber += 3;
            }
            currentNumber = matrix[i][0] + 3;
        }*/


        long sum = 1;
        int rowMatrix = 0;
        int columnMatrix = 0;

        if (row == 1 || column == 1) {
            sum = 1;
        } else {

            boolean firstTimeFlag = true;
            int rowMoving = 1;
            int colMoving = 1;
            int dir=0;
            while (true) {
                rowMatrix += rowMoving;
                columnMatrix += colMoving;

                sum += (rowMatrix+columnMatrix)*3 + 1;

                if (!firstTimeFlag && (rowMatrix == row - 1 || rowMatrix == 0) && (columnMatrix == column - 1 || columnMatrix == 0)) {
                    break;
                }
                firstTimeFlag = false;



                if (columnMatrix+1==column){
                    dir=1;
                    colMoving*=-1;
                    continue;
                }
                else if (columnMatrix==0){
                    dir=0;
                    colMoving*=-1;
                    continue;
                }
                if (dir==0){
                    rowMoving*=-1;
                }
                if (dir==1){
                    rowMoving*=-1;
                }
            }
        }
        System.out.println(sum);
    }
}

