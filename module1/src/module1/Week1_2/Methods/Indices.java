package module1.Week1_2.Methods;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class Indices {
    public static void testInput() {
        String input = "11\n" +
                "2 10 1 3 9 8 7 2 4 6 1";
        System.setIn(new ByteArrayInputStream(input.getBytes()));
    }

    public static void main(String[] args) {
        testInput();
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        scanner.nextLine();
        String inputNumbers = scanner.nextLine();
        String[] arrayNumbers = inputNumbers.split("\\s");
        int[] numbers = new int[arrayNumbers.length];
        for (int i = 0; i < arrayNumbers.length; i++) {
            numbers[i] = Integer.parseInt(arrayNumbers[i]);
        }
        boolean checkArray[] = new boolean[numbers.length];
        ArrayList<Integer> numberslist = new ArrayList<>();
        boolean isCycle = false;
        int oldIndex = 0;
        int index = 0;
        while (index >= 0 && index < n) {
            if (checkArray[index]) {
                oldIndex = index;
                isCycle = true;
                break;
            }

            numberslist.add(index);
            checkArray[index] = true;
            index = numbers[index];
        }
        if (isCycle) {
            for (int i = 0; i < numberslist.size(); i++) {
                if (oldIndex == numberslist.get(i)) {
                    System.out.print("(");
                } else if (i != 0) {
                    System.out.print(" ");
                }
                System.out.print(numberslist.get(i));
            }
            System.out.print(")");
        } else {
            for (int i = 0; i < numberslist.size(); i++) {
                System.out.print(numberslist.get(i));
                System.out.print(" ");
            }

        }
    }
}

