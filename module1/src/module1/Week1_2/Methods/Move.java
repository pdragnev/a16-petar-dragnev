package module1.Week1_2.Methods;

import java.util.Scanner;

public class Move {

    public static void main(String[] args) {
        TestInput.testInput("0\n" +
                "10,20\n" +
                "1 forward 1\n" +
                "2 backwards 1\n" +
                "1 forward 1\n" +
                "exit");
        Scanner scanner = new Scanner(System.in);


        int startingFrom = scanner.nextInt();
        scanner.nextLine();

        String numbers = scanner.nextLine();
        String[] ArrayOfNumbers = numbers.split(",");
        int[] intArrayNumbers = new int[ArrayOfNumbers.length];
        for (int i = 0; i < ArrayOfNumbers.length; i++) {
            intArrayNumbers[i] = Integer.parseInt(ArrayOfNumbers[i]);
        }

        long sumForw = 0;
        long sumBack = 0;

        String StepsDirectionSize = scanner.nextLine();

        while (!"exit".equals(StepsDirectionSize)) {
            String[] moves = StepsDirectionSize.split("\\s");

            for (int i = 0; i < Integer.parseInt(moves[0]); i++) {
                if ("forward".equals(moves[1])) {
                    startingFrom += Integer.parseInt(moves[2]);
                    sumForw += intArrayNumbers[Math.abs(startingFrom % intArrayNumbers.length)];
                } else {
                    startingFrom -= Integer.parseInt(moves[2]);
                    if (startingFrom<0){
                        int size = (int)Math.ceil(Math.abs((double)startingFrom/intArrayNumbers.length));
                        startingFrom+= size*intArrayNumbers.length;
                    }
                    sumBack += intArrayNumbers[Math.abs(startingFrom % intArrayNumbers.length)];
                }
            }

            StepsDirectionSize = scanner.nextLine();
        }

        System.out.println("Forward: " + sumForw);
        System.out.println("Backwards: " + sumBack);
    }
}
