package module1.Week1_2.Practise9_10_19;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

public class Enigma {
    public static void testInput() {
        String input = "28,1,45,255";
        System.setIn(new ByteArrayInputStream(input.getBytes()));
    }

    public static void main(String[] args) {
        testInput();
        Scanner scanner = new Scanner(System.in);
        String[] intput = scanner.nextLine().split(",");
        String[] intputBinary = new String[intput.length];
        for (int i = 0; i < intput.length; i++) {
            String currentConversion = Integer.toBinaryString(Integer.parseInt(intput[i]));
            String nulls="00000000";
            intputBinary[i] = nulls.substring(0,8-currentConversion.length()) + currentConversion;
        }

        String output = "";
        for (int i = 0; i < intputBinary.length; i++) {
            for (int j = 0; j < intputBinary[i].length(); j++) {
                if (i % 2 == 0) {
                    if (j % 2 == 0) {
                        continue;
                    }
                    output += intputBinary[i].charAt(j);
                } else {
                    if (j % 2 != 0) {
                        continue;
                    }
                    output += intputBinary[i].charAt(j);
                }
            }
        }
        System.out.println(output);
    }
}
