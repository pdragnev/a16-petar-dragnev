package module1.Week1_2.Practise9_10_19;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

public class JumpAround {
    public static void testInput() {
        String input = "6 7 3\n" +
                "0 0\n" +
                "2 2\n" +
                "-2 2\n" +
                "3 -1";
        System.setIn(new ByteArrayInputStream(input.getBytes()));
    }

    public static void main(String[] args) {
        testInput();
        Scanner scanner = new Scanner(System.in);
        String[] rowsColsJumps = scanner.nextLine().split("\\s");
        String[] start = scanner.nextLine().split("\\s");

        int rows = Integer.parseInt(rowsColsJumps[0]);
        int cols = Integer.parseInt(rowsColsJumps[1]);
        int jumps = Integer.parseInt(rowsColsJumps[2]);

        int[][] matrix = new int[rows][cols];
        boolean[][] visitedMatrix = new boolean[rows][cols];
        int cell = 1;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                matrix[i][j] = cell++;
            }
        }

            String[][] sequence = new String[jumps][2];
            for (int i = 0; i < sequence.length; i++) {
                sequence[i] = scanner.nextLine().split("\\s");
            }

            int currentRow = Integer.parseInt(start[0]);
            int currentCol = Integer.parseInt(start[1]);
            int rowMoving = 0;
            long sum = 0;
            int nJumps = -1;
            boolean isPeshoFree = false;

            while (true) {
                sum += matrix[currentRow][currentCol];
                visitedMatrix[currentRow][currentCol] = true;
                currentRow += Integer.parseInt(sequence[rowMoving][0]);
                currentCol += Integer.parseInt(sequence[rowMoving][1]);

                rowMoving++;
                nJumps++;
                if (rowMoving == sequence.length) {
                    rowMoving = 0;
                }
                if (!((currentRow >= 0 && currentRow < rows) && (currentCol >= 0 && currentCol < cols))) {
                    isPeshoFree = true;
                    break;
                }
                if (visitedMatrix[currentRow][currentCol]) {
                    break;
                }

            }
            if (isPeshoFree) {
                System.out.printf("escaped %d", sum);
            } else {
                System.out.printf("caught %d", nJumps);
            }

        }
    }

