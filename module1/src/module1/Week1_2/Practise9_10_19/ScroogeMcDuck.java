package module1.Week1_2.Practise9_10_19;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

public class ScroogeMcDuck {
    public static void testInput() {
        String input = "4 3\n" +
                "3 2 4\n" +
                "2 0 3\n" +
                "1 1 5\n" +
                "2 2 5";
        System.setIn(new ByteArrayInputStream(input.getBytes()));
    }

    public static void main(String[] args) {
        testInput();
        Scanner scanner = new Scanner(System.in);
        String [] rowsCols = scanner.nextLine().split("\\s");
        String [][] matrix = new String[Integer.parseInt(rowsCols[0])][Integer.parseInt(rowsCols[1])];
        int [][] matrixValues = new int[Integer.parseInt(rowsCols[0])][Integer.parseInt(rowsCols[1])];
        for (int i = 0; i <matrix.length ; i++) {
            matrix[i] = scanner.nextLine().split("\\s");
        }
        int rowMatrix = 0;
        int colMatrix = 0;
        for (int i = 0; i < matrix.length ; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrixValues[i][j]=Integer.parseInt(matrix[i][j]);
                if (matrixValues[i][j]==0){
                    rowMatrix=i;
                    colMatrix=j;
                }
            }
        }
        int sum=0;
        int rowMoving=0;
        int colMoving=0;
        while(true){
            int dir=0;
            int maxCoins = 0;
            int next_row,next_col;
            int [] xMoves = {0,0,-1,1};
            int [] yMoves = {-1,1,0,0};
            for (int i = 0; i < xMoves.length ; i++) {
                next_row = rowMatrix + xMoves[i];
                next_col = colMatrix + yMoves[i];
                if (isSafe(next_row,next_col,matrixValues,maxCoins)){
                    maxCoins=matrixValues[next_row][next_col];
                    dir=i+1;
                }
            }
            if (dir==0){
                break;
            }
            switch (dir){
                case 1: rowMoving=0; colMoving=-1; break;
                case 2: rowMoving=0; colMoving=1; break;
                case 3: rowMoving=-1; colMoving=0; break;
                case 4: rowMoving=1; colMoving=0; break;
            }
            sum++;
            rowMatrix+=rowMoving;
            colMatrix+=colMoving;
            matrixValues[rowMatrix][colMatrix]-=1;
        }
        System.out.println(sum);


    }
    static boolean isSafe(int x, int y, int[][] sol, int maxCoins) {
        return (x >= 0 && x < sol.length && y >= 0 &&
                y < sol[0].length && sol[x][y]>maxCoins);
    }
}
