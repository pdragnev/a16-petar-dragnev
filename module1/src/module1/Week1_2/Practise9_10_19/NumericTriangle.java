package module1.Week1_2.Practise9_10_19;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

public class NumericTriangle {
    public static void testInput() {
        String input = "1\n" +
                "0\n" +
                "5\n" +
                "6";
        System.setIn(new ByteArrayInputStream(input.getBytes()));
    }

    public static void main(String[] args) {
        testInput();
        Scanner scanner = new Scanner(System.in);
        long firstNumber = scanner.nextInt();
        long secondNumber = scanner.nextInt();
        long thirdNumber = scanner.nextInt();
        long  rows = scanner.nextInt();

        System.out.println(firstNumber);
        System.out.println(secondNumber + " " + thirdNumber);
        for (int i = 2; i < rows; i++) {
            for (int j = 0; j < i+1; j++) {
                long sum=firstNumber+secondNumber+thirdNumber;
                System.out.printf("%d ",sum);
                firstNumber=secondNumber;
                secondNumber=thirdNumber;
                thirdNumber=sum;
            }
            System.out.println();
        }
    }
}
