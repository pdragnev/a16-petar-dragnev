package module1.Week3_4.Collections;

public class Main {
    public static void main(String[] args) {
        MyListG<Integer> list = new MyArrayListG();

        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);

        System.out.println(list.size());
        list.set(1,20);
        System.out.println(list.get(1));

        System.out.println(list.max());
    }
}
