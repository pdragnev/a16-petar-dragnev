package module1.Week3_4.Collections;

public interface MyList extends Iterable<Integer> {

    void add(int element);

    int get(int index);

    void set(int index, int element);

    int size();

    int max();
}
