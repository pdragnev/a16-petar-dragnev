package module1.Week3_4.Collections;

public interface MyListG <E> {
    void add(E element);

    E get(int index);

    void set(int index, E element);

    int size();

    E max();
}
