package module1.Week3_4.Collections;

import java.util.Arrays;

public class MyArrayListG<E extends Comparable<E>> implements MyListG<E> {
    private static final int DEFAULT_CAPACITY = 5;

    private Object[] elements;
    private int size;

    public MyArrayListG() {
        elements = new Object[DEFAULT_CAPACITY];
        size = 0;
    }

    @Override
    public void add(E element) {
        if (size == elements.length) {
            resize();
        }
        this.elements[size++] = element;
    }


    @Override
    public E get(int index) {
        checkIndexInBounds(index);
        return (E) elements[index];
    }

    @Override
    public void set(int index, E element) {
        checkIndexInBounds(index);
        elements[index] = element;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public E max() {
        if (size==0){
            throw new IllegalArgumentException();
        }
        E maxElement= (E)elements[0];
        for (int i = 0; i < size; i++) {
            E casterElement = (E) elements[i];
            if (casterElement.compareTo(maxElement)>0){
                maxElement=casterElement;
            }
        }
        return maxElement;
    }

    private void resize() {
        int newCapacity = elements.length * 2;
        elements = Arrays.copyOf(elements, newCapacity);
    }

    private void checkIndexInBounds(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
    }
}
