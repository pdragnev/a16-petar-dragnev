package module1.Week3_4.Collections;

import java.util.Arrays;
import java.util.Iterator;

public class MyArrayList implements MyList {

    private static final int DEFAULT_CAPACITY = 5;

    private int[] elements;
    private int size;

    public MyArrayList() {
        elements = new int[DEFAULT_CAPACITY];
        size = 0;
    }

    @Override
    public void add(int element) {
        if (size == elements.length) {
            resize();
        }
        this.elements[size++] = element;
    }

    @Override
    public int get(int index) {
        checkIndexInBounds(index);
        return elements[index];
    }

    @Override
    public void set(int index, int element) {
        checkIndexInBounds(index);
        elements[index] = element;
    }


    @Override
    public int size() {
        return size;
    }

    @Override
    public int max() {
        int maxElement = elements[0];
        for (int i = 0; i < size; i++) {
            if (elements[i] > maxElement) {
                maxElement = elements[i];
            }
        }
        return maxElement;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new MyArrayListIterator();
    }

    private void resize() {
        int newCapacity = elements.length * 2;
        elements = Arrays.copyOf(elements, newCapacity);
    }

    private void checkIndexInBounds(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
    }

    private class MyArrayListIterator implements Iterator<Integer> {
        private int currentIndex;

        MyArrayListIterator() {
            currentIndex = 0;
        }

        @Override
        public boolean hasNext() {
            return currentIndex < size;
        }

        @Override
        public Integer next() {
            if (hasNext()) {
                return elements[currentIndex++];
            }
            throw new IllegalArgumentException();
        }
    }
}