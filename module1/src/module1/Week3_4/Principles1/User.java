package module1.Week3_4.Principles1;


public class User {
    private int id;

    public User(int id) {
        this.id = id;
    }

    public void saveWebLink(){
        System.out.println("User saveWebLink called");
    }

    public void saveMovie(){
        System.out.println("User saveMovie called");
    }
    public void saveBook(){
        System.out.println("User saveBook called");
    }
    public void rateBookmark(){
        System.out.println("User rateBookmark called");
    }
    public void postAReview(){
        System.out.println("User postAReview called");
    }

    @Override
    public String toString() {
        return String.format("User id=%d",id);
    }
}
