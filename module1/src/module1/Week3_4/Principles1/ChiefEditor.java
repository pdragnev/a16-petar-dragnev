package module1.Week3_4.Principles1;

public class ChiefEditor extends Editor {

    public ChiefEditor(int id) {
        super(id);
    }

    @Override
    public String toString() {
        return "ChiefEditor from " + super.toString();
    }
}
