package module1.Week3_4.Principles1;

public class Main {
    public static void main(String[] args) {
        User user1 = new User(1);
        User user2 = new Staff(2);
        User user3 = new Editor(3);
        User user4 = new EmailAdmin(4);
        User user5 = new ChiefEditor(5);

        System.out.println(user1.toString());
        System.out.println(user2.toString());
        System.out.println(user3.toString());
        System.out.println(user4.toString());
        System.out.println(user5.toString());

    }
}