package module1.Week3_4.Principles1;

public class EmailAdmin extends Staff {

    public EmailAdmin(int id) {
        super(id);
    }

    @Override
    public String toString() {
        return "EmailAdmin from " + super.toString();
    }
}
