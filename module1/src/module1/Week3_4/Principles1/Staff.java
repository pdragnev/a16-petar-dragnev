package module1.Week3_4.Principles1;

public class Staff extends User {

    public Staff(int id) {
        super(id);
    }

    @Override
    public void postAReview() {
        System.out.println("Staff posAReview called");
    }

    @Override
    public String toString() {
        return "Staff " + super.toString();
    }
}
