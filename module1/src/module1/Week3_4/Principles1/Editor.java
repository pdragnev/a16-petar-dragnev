package module1.Week3_4.Principles1;

public class Editor extends Staff{

    public Editor(int id) {
        super(id);
    }

    @Override
    public String toString() {
        return "Editor from " + super.toString();
    }
}
