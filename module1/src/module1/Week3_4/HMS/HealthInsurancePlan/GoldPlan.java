package module1.Week3_4.HMS.HealthInsurancePlan;

public class GoldPlan extends HealthInsurancePlan {
    private static final double GOLD_COVERAGE = 0.8;
    private static final double GOLD_PREMIUM = 0.07;

    @Override
    public double getCoverage() {
        return GOLD_COVERAGE;
    }

    @Override
    public double computeMonthlyPremium(double salary) {
        return GOLD_PREMIUM * salary;
    }

    @Override
    public  double computeMonthlyPremium(double salary,  int age,  boolean smoking)  {
        return  GOLD_PREMIUM * salary + getOfferedBy().computeMonthlyPremium(this,age,smoking);
    }

}
