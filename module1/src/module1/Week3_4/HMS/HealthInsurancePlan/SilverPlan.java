package module1.Week3_4.HMS.HealthInsurancePlan;

public class SilverPlan extends HealthInsurancePlan {
    private static final double SILVER_COVERAGE = 0.7;
    private static final double SILVER_PREMIUM = 0.06;

    @Override
    public double getCoverage() {
        return SILVER_COVERAGE;
    }

    @Override
    public double computeMonthlyPremium(double salary) {
        return SILVER_PREMIUM * salary;
    }

    @Override
    public  double computeMonthlyPremium(double salary,  int age,  boolean smoking)  {
        return  SILVER_PREMIUM * salary + getOfferedBy().computeMonthlyPremium(this,age,smoking);
    }
}
