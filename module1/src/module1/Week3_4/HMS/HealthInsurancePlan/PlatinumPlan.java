package module1.Week3_4.HMS.HealthInsurancePlan;

public class PlatinumPlan extends HealthInsurancePlan {

    private static final double PLAT_COVERAGE = 0.9;
    private static final double PLAT_PREMIUM = 0.08;

    @Override
    public double getCoverage() {
        return PLAT_COVERAGE;
    }

    @Override
    public double computeMonthlyPremium(double salary) {
        return PLAT_PREMIUM * salary;
    }

    @Override
    public  double computeMonthlyPremium(double salary,  int age,  boolean smoking)  {
        return  PLAT_PREMIUM * salary + getOfferedBy().computeMonthlyPremium(this,age,smoking);
    }


}
