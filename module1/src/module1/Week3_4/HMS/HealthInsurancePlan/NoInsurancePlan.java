package module1.Week3_4.HMS.HealthInsurancePlan;

public class NoInsurancePlan extends HealthInsurancePlan {

    @Override
    public double getCoverage() {
        return 0;
    }

    @Override
    public double computeMonthlyPremium(double salary) {
        return 0;
    }

    @Override
    public double computeMonthlyPremium(double salary, int age, boolean smoking) {
        return 0;
    }
}
