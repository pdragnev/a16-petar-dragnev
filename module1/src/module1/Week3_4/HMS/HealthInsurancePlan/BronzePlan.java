package module1.Week3_4.HMS.HealthInsurancePlan;

public class BronzePlan extends HealthInsurancePlan {
    private static final double BRONZE_COVERAGE = 0.6;
    private static final double BRONZE_PREMIUM = 0.05;

    @Override
    public double getCoverage() {
        return BRONZE_COVERAGE;
    }

    @Override
    public double computeMonthlyPremium(double salary) {
        return BRONZE_PREMIUM * salary;
    }

    @Override
    public  double computeMonthlyPremium(double salary,  int age,  boolean smoking)  {
        return  BRONZE_PREMIUM * salary + getOfferedBy().computeMonthlyPremium(this,age,smoking);
    }

}
