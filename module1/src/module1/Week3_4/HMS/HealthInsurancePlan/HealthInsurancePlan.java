package module1.Week3_4.HMS.HealthInsurancePlan;


import module1.Week3_4.HMS.contracts.InsuranceBrand;

public abstract class HealthInsurancePlan{

    private InsuranceBrand insuranceBrand;

    public abstract double getCoverage();

    public abstract double computeMonthlyPremium(double salary);

    public abstract double computeMonthlyPremium(double salary,  int age,  boolean smoking);


    public void setOfferedBy(InsuranceBrand insuranceBrand){
        this.insuranceBrand = insuranceBrand;
    }
    public InsuranceBrand getOfferedBy (){
        return insuranceBrand;
    }


}

