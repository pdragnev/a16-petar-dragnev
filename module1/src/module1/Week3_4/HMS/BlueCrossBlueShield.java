package module1.Week3_4.HMS;

import module1.Week3_4.HMS.HealthInsurancePlan.*;
import module1.Week3_4.HMS.contracts.InsuranceBrand;


public class BlueCrossBlueShield implements InsuranceBrand {
    private static final int AGE_PREMIUM_INCREASE = 55;

    private static final int PLAT_AGE_PREMIUM_INCREASE = 200;
    private static final int PLAT_SMOKE_PREMIUM_INCREASE = 100;

    private static final int GOLD_AGE_PREMIUM_INCREASE = 150;
    private static final int GOLD_SMOKE_PREMIUM_INCREASE = 90;

    private static final int SILVER_AGE_PREMIUM_INCREASE = 100;
    private static final int SILVER_SMOKE_PREMIUM_INCREASE = 80;

    private static final int BRONZE_AGE_PREMIUM_INCREASE = 50;
    private static final int BRONZE_SMOKE_PREMIUM_INCREASE = 70;

    private long id;
    private String name;

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }


    @Override
    public double computeMonthlyPremium(HealthInsurancePlan insurancePlan, int age, boolean smoking) {
        double offer=0;

            if (insurancePlan instanceof PlatinumPlan) {
                offer+= age> AGE_PREMIUM_INCREASE ? PLAT_AGE_PREMIUM_INCREASE : 0;
                offer+= smoking? PLAT_SMOKE_PREMIUM_INCREASE : 0;
            } else if (insurancePlan instanceof GoldPlan) {
                offer+= age>AGE_PREMIUM_INCREASE? GOLD_AGE_PREMIUM_INCREASE : 0;
                offer+= smoking? GOLD_SMOKE_PREMIUM_INCREASE : 0;
            } else if (insurancePlan instanceof SilverPlan) {
                offer+= age>AGE_PREMIUM_INCREASE? SILVER_AGE_PREMIUM_INCREASE : 0;
                offer+= smoking? SILVER_SMOKE_PREMIUM_INCREASE : 0;
            } else if (insurancePlan instanceof BronzePlan) {
                offer+= age>AGE_PREMIUM_INCREASE? BRONZE_AGE_PREMIUM_INCREASE : 0;
                offer+= smoking? BRONZE_SMOKE_PREMIUM_INCREASE : 0;

            }
        return offer;
    }
}

