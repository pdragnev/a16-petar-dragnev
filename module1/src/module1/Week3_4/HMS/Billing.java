package module1.Week3_4.HMS;

import module1.Week3_4.HMS.HealthInsurancePlan.*;

import module1.Week3_4.HMS.Hospital.User;

public class Billing {
    private final static int PLAT_DISCOUNT = 50;
    private final static int GOLD_DISCOUNT = 40;
    private final static int SILVER_DISCOUNT = 30;
    private final static int BRONZE_DISCOUNT = 25;
    private final static int DEFAULT_DISCOUNT = 20;
    private static final int INSURANCE_PATIENT_BILL_ARR_LEN = 2;

    public static double[] computePaymentAmount(User patient, double amount) {
        //first element would have the amount that the insurance company would pay 
        //second element would have the amount that patient has to pay
        double[] bill = new double[INSURANCE_PATIENT_BILL_ARR_LEN];
        HealthInsurancePlan currentPlan = patient.getInsurancePlan();
        double insuranceCompanyPay;
        double clientPay;
        insuranceCompanyPay = amount * currentPlan.getCoverage();
        clientPay = amount - insuranceCompanyPay;
        if (currentPlan instanceof PlatinumPlan) {
            clientPay -= PLAT_DISCOUNT;
        } else if (currentPlan instanceof GoldPlan) {
            clientPay -= GOLD_DISCOUNT;
        } else if (currentPlan instanceof SilverPlan) {
            clientPay -= SILVER_DISCOUNT;
        } else if (currentPlan instanceof BronzePlan) {
            clientPay -= BRONZE_DISCOUNT;
        } else if (currentPlan instanceof NoInsurancePlan){
            insuranceCompanyPay = 0;
            clientPay = amount - DEFAULT_DISCOUNT;
        }

        if (clientPay < 0) {
            clientPay = 0;
        }

        bill[0] = insuranceCompanyPay;
        bill[1] = clientPay;

        return bill;
    }
}
