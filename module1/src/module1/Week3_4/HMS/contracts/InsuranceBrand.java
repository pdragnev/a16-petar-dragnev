package module1.Week3_4.HMS.contracts;

import module1.Week3_4.HMS.HealthInsurancePlan.HealthInsurancePlan;

public interface InsuranceBrand {

    double computeMonthlyPremium(HealthInsurancePlan insurancePlan, int age, boolean smoking);
}
