package module1.Week3_4.HMS;

import module1.Week3_4.HMS.HealthInsurancePlan.*;
import module1.Week3_4.HMS.Hospital.*;
import module1.Week3_4.HMS.contracts.InsuranceBrand;

public class Main {

    public static final String BILL_MSG_FORMAT = "Applying the coverage of %.2f$, patient's part would be %.2f$.";

    public static void main(String[] args) {
        User user1 = new User("Asmon", "Gold", "Male", "asmon@gmail.com",true,new PlatinumPlan(),60,true);
        User user2 = new Staff("Petar", "Xai", "Male", "petarX@gmail.com",
                true, null, 15, "ala bala",2500,60,true);
        User user3 = new Patient("Doncho", "Minkov", "Male", "dminkov@gmail.com", false, new GoldPlan(),60,true);
        User user4 = new Doctor("Petar", "Leadership", "Male",
                "petarL@gmail.com", true, new PlatinumPlan(), 5, "super doktor",2500,"Pediatritian",60,true);
        User user5 = new Nurse("Minka", "Kostova", "Female",
                "mkostova@gmail.com", false, null, 22,"super sestra", 250,60,true);


        double [] bill = Billing.computePaymentAmount(user3,1000);
        System.out.printf(BILL_MSG_FORMAT,bill[0],bill[1]);

        System.out.println();
        System.out.println("===================");

        System.out.println(user5.getInsurancePlan().computeMonthlyPremium(((Staff)user5).getSalary()));



        System.out.println(System.lineSeparator() + "=======================================");

        User staff =  new  User();
        InsuranceBrand insuranceBrand =  new  BlueCrossBlueShield();
        HealthInsurancePlan insurancePlan =  new  PlatinumPlan();

        insurancePlan.setOfferedBy(insuranceBrand);
        staff.setInsurancePlan(insurancePlan);
        System.out.println(insurancePlan.computeMonthlyPremium(5000,  56,  true));

    }
}
