package module1.Week3_4.HMS.Hospital;

import module1.Week3_4.HMS.HealthInsurancePlan.HealthInsurancePlan;
import module1.Week3_4.HMS.HealthInsurancePlan.NoInsurancePlan;

public class User {
    private static long idCount = 1;
    private long id;
    private String firstName;
    private String lastName;
    private String gender;
    private String email;
    private boolean isInsured;
    private HealthInsurancePlan insurancePlan;
    private int age;
    private boolean isSmoking;


    public User() {
        setId(idCount++);
    }

    public User(String firstName, String lastName, String gender, String email, boolean isInsured, HealthInsurancePlan insurancePlan, int age, boolean isSmoking) {
        setId(idCount++);
        setFirstName(firstName);
        setLastName(lastName);
        setGender(gender);
        setEmail(email);
        setInsured(isInsured);
        setInsurancePlan(insurancePlan);
        setAge(age);
        setSmoking(isSmoking);
    }

    public int getAge() {
        return age;
    }

    private void setAge(int age) {
        this.age = age;
    }

    public boolean isSmoking() {
        return isSmoking;
    }

    private void setSmoking(boolean smoking) {
        isSmoking = smoking;
    }

    public boolean isInsured() {
        return isInsured;
    }

    private void setInsured(boolean insured) {
        isInsured = insured;
    }

    public HealthInsurancePlan getInsurancePlan() {
        return insurancePlan;
    }

    public void setInsurancePlan(HealthInsurancePlan insurancePlan) {

        if (insurancePlan == null) {
            this.insurancePlan = new NoInsurancePlan();
            return;
        }
        this.insurancePlan = insurancePlan;
    }

    public long getId() {
        return id;
    }

    private void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    private void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    private void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    private void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    private void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return String.format("ID:%d" + System.lineSeparator() +
                "Name: %s %s" + System.lineSeparator() +
                "Gender: %s" + System.lineSeparator() +
                "Email %s", getId(), getFirstName(), getLastName(), getGender(), getEmail());
    }
}
