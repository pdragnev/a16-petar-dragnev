package module1.Week3_4.HMS.Hospital;

import module1.Week3_4.HMS.HealthInsurancePlan.HealthInsurancePlan;

public class Patient extends User {

    private static final String INSURED_MSG = "Patient is insured";
    private static final String NOT_INSURED_MSG = "Patient is not insured";

    public Patient(String firstName, String lastName, String gender, String email, boolean isInsured, HealthInsurancePlan insurancePlan, int age, boolean isSmoking) {
        super(firstName, lastName, gender, email, isInsured, insurancePlan, age, isSmoking);
    }

    @Override
    public String toString() {
        return String.format("%s" + System.lineSeparator() + "Detailed information below:" + System.lineSeparator() + super.toString(), isInsured() ? INSURED_MSG : NOT_INSURED_MSG);

    }
}
