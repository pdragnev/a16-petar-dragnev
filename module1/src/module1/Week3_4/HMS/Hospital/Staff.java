package module1.Week3_4.HMS.Hospital;

import module1.Week3_4.HMS.HealthInsurancePlan.HealthInsurancePlan;

public class Staff extends User {
    private int yearsOfExperience;
    private String description;
    private double salary;

    public Staff(String firstName, String lastName, String gender, String email, boolean isInsured, HealthInsurancePlan insurancePlan,
                 int yearsOfExperience, String description, double salary, int age, boolean isSmoking) {
        super(firstName, lastName, gender, email, isInsured, insurancePlan, age, isSmoking);
        setYearsOfExperience(yearsOfExperience);
        setDescription(description);
        setSalary(salary);
    }

    public int getYearsOfExperience() {
        return yearsOfExperience;
    }

    private void setYearsOfExperience(int yearsOfExperience) {
        this.yearsOfExperience = yearsOfExperience;
    }

    public String getDescription() {
        return description;
    }

    private void setDescription(String description) {
        this.description = description;
    }

    public double getSalary() {
        return salary;
    }

    private void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return String.format("Staff member with %d years of experience and salary of %.2f"
                        + System.lineSeparator() + "Description: " + System.lineSeparator() +
                        "%s" + System.lineSeparator() + "More information below:" +
                        System.lineSeparator() + super.toString(),
                getYearsOfExperience(), getSalary(), getDescription());

    }
}
