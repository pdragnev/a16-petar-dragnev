package module1.Week3_4.HMS.Hospital;

import module1.Week3_4.HMS.HealthInsurancePlan.HealthInsurancePlan;

public class Nurse extends Staff {

    public Nurse(String firstName, String lastName, String gender, String email, boolean isInsured, HealthInsurancePlan insurancePlan,
                 int yearsOfExperience, String description, double salary, int age, boolean isSmoking) {
        super(firstName, lastName, gender, email, isInsured, insurancePlan, yearsOfExperience, description, salary, age, isSmoking);
    }

    @Override
    public String toString() {
        return "Nurse is" + super.toString();
    }
}
