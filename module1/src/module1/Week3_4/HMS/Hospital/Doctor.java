package module1.Week3_4.HMS.Hospital;

import module1.Week3_4.HMS.HealthInsurancePlan.HealthInsurancePlan;

public class Doctor extends Staff {
    private String specialisation;

    public Doctor(String firstName, String lastName, String gender, String email, boolean isInsured, HealthInsurancePlan insurancePlan,
                  int yearsOfExperience, String description, double salary,
                  String specialisation, int age, boolean isSmoking) {
        super(firstName, lastName, gender, email, isInsured, insurancePlan, yearsOfExperience, description, salary, age, isSmoking);
        setSpecialisation(specialisation);
    }

    public String getSpecialisation() {
        return specialisation;
    }

    private void setSpecialisation(String specialisation) {
        this.specialisation = specialisation;
    }

    @Override
    public String toString() {
        return String.format("Doctor with %s is " + super.toString(), getSpecialisation());
    }
}
