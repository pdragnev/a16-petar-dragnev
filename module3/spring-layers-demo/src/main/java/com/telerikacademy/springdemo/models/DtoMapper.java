package com.telerikacademy.springdemo.models;

import com.telerikacademy.springdemo.models.Beer;
import com.telerikacademy.springdemo.models.BeerDto;
import com.telerikacademy.springdemo.models.Style;
import com.telerikacademy.springdemo.repositories.BeersRepository;
import com.telerikacademy.springdemo.repositories.StylesRepository;
import com.telerikacademy.springdemo.services.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DtoMapper {
    private StyleService styleService;

    @Autowired
    public DtoMapper(StyleService styleService) {
        this.styleService = styleService;
    }

    public Beer fromDto(BeerDto beerDto) {
        Beer beer = new Beer(beerDto.getName(),beerDto.getAbv());
        Style style = styleService.getById(beerDto.getStyleId());
        beer.setStyle(style);
        return beer;
    }
}
