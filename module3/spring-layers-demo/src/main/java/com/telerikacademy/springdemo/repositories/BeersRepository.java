package com.telerikacademy.springdemo.repositories;

import com.telerikacademy.springdemo.models.Beer;

import java.util.List;

public interface BeersRepository {
    Beer create(Beer beer);

    List<Beer> getAll();

    Beer getById(int id);

    boolean checkBeerExists(String name);

    Beer update(int id,Beer beer);

    void delete(int id);
}
