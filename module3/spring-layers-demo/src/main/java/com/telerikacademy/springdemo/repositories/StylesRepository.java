package com.telerikacademy.springdemo.repositories;

import com.telerikacademy.springdemo.models.Style;

import java.util.List;

public interface StylesRepository {
    List<Style> getAll();

    Style getById(int id);
}
