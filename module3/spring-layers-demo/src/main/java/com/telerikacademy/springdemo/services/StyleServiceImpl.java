package com.telerikacademy.springdemo.services;

import com.telerikacademy.springdemo.models.Style;
import com.telerikacademy.springdemo.repositories.StylesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StyleServiceImpl implements StyleService{
    private StylesRepository stylesRepository;

    @Autowired
    public StyleServiceImpl(StylesRepository stylesRepository) {
        this.stylesRepository = stylesRepository;
    }

    @Override
    public List<Style> getAll() {
        return null;
    }

    @Override
    public Style getById(int id) {
        return null;
    }
}
