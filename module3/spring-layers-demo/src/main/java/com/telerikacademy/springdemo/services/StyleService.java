package com.telerikacademy.springdemo.services;

import com.telerikacademy.springdemo.models.Style;

import java.util.List;

public interface StyleService {
    List<Style> getAll();

    Style getById(int id);
}
