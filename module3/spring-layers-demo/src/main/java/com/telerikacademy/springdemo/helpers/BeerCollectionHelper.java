package com.telerikacademy.springdemo.helpers;

import com.telerikacademy.springdemo.models.Beer;

import java.util.List;
import java.util.stream.Collectors;

public class BeerCollectionHelper {
    public static List<Beer> filterByName(List<Beer> beers, String name){
        return beers.stream().
                filter(beer -> beer.getName().toLowerCase().contains(name.toLowerCase()))
                .collect(Collectors.toList());
    }

    public static List<Beer> filterByStyle(List<Beer> beers, String style){
        return beers.stream().
                filter(beer -> beer.getStyle().getName().toLowerCase().contains(style.toLowerCase()))
                .collect(Collectors.toList());
    }
}
