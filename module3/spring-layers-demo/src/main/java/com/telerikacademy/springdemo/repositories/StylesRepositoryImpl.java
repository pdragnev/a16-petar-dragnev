package com.telerikacademy.springdemo.repositories;

import com.telerikacademy.springdemo.exceptions.EntityNotFoundException;
import com.telerikacademy.springdemo.models.Style;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class StylesRepositoryImpl implements StylesRepository {
    private List<Style> styles;

    @Autowired
    public StylesRepositoryImpl() {
        styles = new ArrayList<>();
        styles.add(new Style(0,"Special Ale"));
        styles.add(new Style(1,"English Porter"));
        styles.add(new Style(2, "Indian Pale Ale"));
    }

    @Override
    public List<Style> getAll() {
        return styles;
    }

    @Override
    public Style getById(int id) {
        return styles.stream()
                .filter(style -> style.getId() == id)
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("Styles","id",String.valueOf(id)));
    }
}
