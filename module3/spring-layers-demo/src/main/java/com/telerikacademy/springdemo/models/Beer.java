package com.telerikacademy.springdemo.models;

import javax.validation.constraints.*;

public class Beer {


    private int id;
    private String name;
    private Style style;
    private double abv;

    public Beer() {

    }

    public Beer( String name, double abv) {
        this.abv = abv;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }
}
