package com.telerikacademy.springdemo.controllers;

import com.telerikacademy.springdemo.exceptions.DuplicateEntityException;
import com.telerikacademy.springdemo.exceptions.EntityNotFoundException;
import com.telerikacademy.springdemo.helpers.BeerCollectionHelper;
import com.telerikacademy.springdemo.models.Beer;
import com.telerikacademy.springdemo.models.BeerDto;
import com.telerikacademy.springdemo.models.DtoMapper;
import com.telerikacademy.springdemo.services.BeersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/beers")
public class BeersController {
    private BeersService service;
    private DtoMapper mapper;

    @Autowired
    public BeersController(BeersService service,DtoMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    public List<Beer> getAll(@RequestParam(defaultValue = "") String name,
                             @RequestParam(defaultValue = "") String style) {
        List<Beer> beers = service.getAll();

        if (!name.isEmpty()){
            beers = BeerCollectionHelper.filterByName(beers,name);
        }

        if (!style.isEmpty()){
            beers = BeerCollectionHelper.filterByStyle(beers,style);
        }
        return beers;
    }

    @GetMapping("/{id}")
    public Beer getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Beer create(@RequestBody @Valid BeerDto beerDto) {
        try {
            Beer newBeer = mapper.fromDto(beerDto);
            return service.create(newBeer);
        } catch (EntityNotFoundException nfe) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, nfe.getMessage());
        }catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Beer update(@PathVariable int id,@RequestBody @Valid BeerDto beerDto) {
        try {
            Beer beer = mapper.fromDto(beerDto);
            return service.update(id,beer);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
