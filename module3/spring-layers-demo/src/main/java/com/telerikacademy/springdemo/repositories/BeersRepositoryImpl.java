package com.telerikacademy.springdemo.repositories;

import com.telerikacademy.springdemo.exceptions.EntityNotFoundException;
import com.telerikacademy.springdemo.models.Beer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class BeersRepositoryImpl implements BeersRepository {
    private static int nextId = 0;
    private List<Beer> beers;

    @Autowired
    public BeersRepositoryImpl(StylesRepository stylesRepository) {
        beers = new ArrayList<>();
        Beer beer = new Beer("Glarus Pale Ale", 4.7);
        beer.setId(nextId++);
        beer.setStyle(stylesRepository.getById(0));
        beers.add(beer);
        beer = new Beer("Shumensko", 4.7);
        beer.setId(nextId++);
        beer.setStyle(stylesRepository.getById(1));
        beers.add(beer);
        beer = new Beer("Zagorka", 4.7);
        beer.setId(nextId++);
        beer.setStyle(stylesRepository.getById(2));
        beers.add(beer);
    }

    @Override
    public Beer create(Beer beer) {
        beer.setId(nextId++);
        beers.add(beer);
        return beer;
    }

    @Override
    public List<Beer> getAll() {
        return beers;
    }

    @Override
    public Beer getById(int id) {
        return beers.stream()
                .filter(beer -> beer.getId() == id)
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("Beer",String.valueOf(id)));
    }

    @Override
    public boolean checkBeerExists(String name) {
        return beers.stream()
                .anyMatch(beer -> beer.getName().equals(name));
    }

    @Override
    public Beer update(int id,Beer beer) {
        Beer beerToUpdate = getById(id);
        beerToUpdate.setName(beer.getName());
        beerToUpdate.setAbv(beer.getAbv());
        return beerToUpdate;
    }

    @Override
    public void delete(int id) {
        Beer beerToDelete = getById(id);
        beers.remove(beerToDelete);
    }
}
