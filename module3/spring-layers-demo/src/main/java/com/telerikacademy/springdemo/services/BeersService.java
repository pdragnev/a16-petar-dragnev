package com.telerikacademy.springdemo.services;

import com.telerikacademy.springdemo.models.Beer;

import java.util.List;

public interface BeersService {
    Beer create(Beer beer);

    List<Beer> getAll();

    Beer getById(int id);

    Beer update(int id,Beer beer);

    void delete(int id);
}
