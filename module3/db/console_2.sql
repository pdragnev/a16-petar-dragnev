use telerikacademy;
# Write a SQL query to find all information about all departments (use "TelerikAcademy" database).
select *
from departments;

# Write a SQL query to find all department names.
select Name as name
from departments;

# Write a SQL query to find the salary of each employee.
select FirstName as name, round(Salary, 2)
from employees;

# Write a SQL to find the full name of each employee.
select FirstName, LastName
from employees;

# Write a SQL query to find the email addresses of each employee (by his first and last name).
# Consider that the mail domain is telerik.com. Emails should look like “John.Doe@telerik.com".
# The produced column should be named "Full Email Addresses".
select CONCAT(FirstName, '.', LastName, '@telerik.com')
from employees;


# Write a SQL query to find all different employee salaries.
select distinct Salary
from employees;

# Write a SQL query to find all information about the employees whose job title is “Sales Representative“.
select *
from employees
where JobTitle = 'Sales Representative';

# Write an SQL query to find all employees whose salary is bigger than their manager's.
select *
from employees emp
where Salary > (select managers.Salary
                from employees managers
                where managers.EmployeeID = emp.ManagerID);

# Write a SQL query to find the names of all employees whose first name starts with "SA".
select FirstName, LastName
from employees
where FirstName like 'sa%';


# Write a SQL query to find the names of all employees whose last name contains "ei".
select FirstName, LastName
from employees
where LastName like '%ei%';

# Write a SQL query to find the salary of all employees whose salary is in the range [20000…30000].
select *
from employees
where Salary between 20000 and 30000;

# Write a SQL query to find the names of all employees whose salary is 25000, 14000, 12500 or 23600.
select FirstName, LastName, Salary
from employees
where Salary in (25000, 14000, 12500, 23600);

# Write a SQL query to find all employees that do not have manager.
select *
from employees
where ManagerID IS NULL;

# Write a SQL query to find the names of all employees who are hired before their managers.
select *
from employees
where HireDate < (select managers.HireDate from employees managers where managers.EmployeeID = employees.ManagerID);

# Write a SQL query to find all employees that have salary more than 50000. Order them in decreasing order by salary.
select *
from employees
where Salary > 50000
order by Salary DESC;

# Write a SQL query to find the top 5 best paid employees.
select *
from employees
order by Salary DESC
limit 5;

# Write a SQL query to find all employees and their address.
select FirstName, LastName, a.AddressText, t.Name
from employees
         inner join addresses a on employees.AddressID = a.AddressID
         inner join towns t on a.TownID = t.TownID;

# Write a SQL query to find all employees whose MiddleName is the same as the first letter of their town.
select FirstName, MiddleName, LastName, a.AddressText, t.Name
from employees
         inner join addresses a on employees.AddressID = a.AddressID
         inner join towns t on a.TownID = t.TownID
where LEFT(MiddleName, 1) = LEFT(t.Name, 1);

# Write a SQL query to find all employees that have manager, along with their manager.
select e.FirstName, e.LastName, e.ManagerID, manager.EmployeeID, manager.FirstName, manager.LastName
from employees e
         inner join employees manager ON e.ManagerID = manager.EmployeeID;

# Write a SQL query to find all employees that have manager, along with their manager and their address.
select e.FirstName,
       e.LastName,
       e.ManagerID,
       manager.EmployeeID,
       manager.FirstName,
       manager.LastName,
       a.AddressText,
       t.Name
from employees e
         inner join employees manager ON e.ManagerID = manager.EmployeeID
         inner join addresses a on manager.AddressID = a.AddressID
         inner join towns t on a.TownID = t.TownID;

# Write a SQL query to find all departments and all town names as a single list.
select Name
from departments
union
select Name
from towns;

# Write a SQL query to find all the employees and the manager for each of them along with the employees that do not have manager.
select e.FirstName, e.LastName, e.ManagerID, manager.EmployeeID, manager.FirstName, manager.LastName
from employees e
         left join employees manager ON e.ManagerID = manager.EmployeeID;

# Write a SQL query to find the names of all employees from the departments "Sales" and "Finance" whose hire year is between 1995 and 2005.
select e.FirstName, e.LastName, d.Name, e.HireDate, d.Name
from employees e
         inner join departments d on e.DepartmentID = d.DepartmentID
where (d.Name like '%sales%' OR d.Name like '%finance%')
  AND year(e.HireDate) between 1995 AND 2005;

# SECOND PRACTISE HERE

# Write a SQL query to find the average salary of employees who have been hired before year 2000 incl. Round it down to the nearest integer.
select avg(e.Salary)
from employees e
where year(e.HireDate) < 2000;

# Write a SQL query to find all town names that end with a vowel (a,o,u,e,i).
select t.Name
from towns t
where RIGHT(t.Name, 1) in ('a', 'o', 'i', 'e', 'u');

# Write a SQL query to find all town names that start with a vowel (a,o,u,e,i).
select t.Name
from towns t
where LEFT(t.Name, 1) in ('a', 'o', 'i', 'e', 'u');

# Write a SQL query to find the name and the length of the name of the town with the longest name.
select t.Name, LENGTH(t.Name) lenght
from towns t
where LENGTH(t.Name) = (select MAX(LENGTH(towns.Name)) from towns);

# Write a SQL query to find the name and the length of the name of the town with the shortest name.
select t.Name, LENGTH(t.Name) lenght
from towns t
where LENGTH(t.Name) = (select MIN(LENGTH(towns.Name)) from towns);

# Write a SQL query to find all employees with even IDs.
select *
from employees e
where e.EmployeeID % 2 = 0;

# Write a SQL query to find the names and salaries of the employees that take the minimal salary in the company.
select e.FirstName, e.LastName, e.Salary
from employees e
where e.Salary = (select MIN(employees.Salary) from employees);

# Write a SQL query to find the names and salaries of the employees that have a salary that is up to 10% higher than the minimal salary for the company.
select e.FirstName, e.LastName, e.Salary
from employees e
where e.Salary <= (select MIN(employees.Salary) from employees) * 1.1;

# Write a SQL query to find the full name, salary and department of the employees that take the minimal salary in their department.

select FirstName, LastName, e1.Salary, minsalary.dep_name
from employees e1
         join
     (select MIN(e.Salary) salary, d.Name as dep_name, d.DepartmentID as dep_id
      from employees e
               inner join departments d on e.DepartmentID = d.DepartmentID
      group by d.DepartmentID) minsalary
     on minsalary.dep_id = e1.DepartmentID AND e1.Salary = minsalary.salary;


# Write a SQL query to find the average salary in the department ##
# Write a SQL query to find the average salary  in the "Sales" department.
# Write a SQL query to find the number of employees in the "Sales" department.
# Write a SQL query to find the number of all employees that have manager.
# Write a SQL query to find the number of all employees that have no manager.
# Write a SQL query to find all departments and the average salary for each of them.
select AVG(e.Salary), d.Name
from employees e
         inner join departments d on e.DepartmentID = d.DepartmentID
group by d.Name;

# Write a SQL query to find all project that took less than a month (30 days) to complete.
select p.Name, p.StartDate, p.EndDate, DATEDIFF(p.EndDate, p.StartDate)
from projects p
where DATEDIFF(p.EndDate, p.StartDate) < 30;

# Write a SQL query to find the count of all employees in each department and for each town.
select count(*), d.Name
from employees
         join departments d on employees.DepartmentID = d.DepartmentID
group by d.DepartmentID
union
select count(*), t.Name
from employees
         join addresses a on employees.AddressID = a.AddressID
         join towns t on a.TownID = t.TownID
group by t.TownID;
# Write a SQL query to find all managers that have exactly 5 employees. Display their first name and last name.
select m.FirstName, m.LastName
from employees as m
where EmployeeID in (
    select ManagerID
    from employees
    group by ManagerID
    having count(m.EmployeeID) = 5);
# Write a SQL query to find all employees along with their managers. For employees that do not have manager display the value "(no manager)".
select e.FirstName,
       e.LastName,
       CONCAT(ifnull(m.FirstName, 'no'), ' ', ifnull(m.LastName, 'manager')) managername
from employees e
         left join employees m on e.ManagerID = m.EmployeeID;
# Write a SQL query to find the names of all employees whose last name is exactly 5 characters long.
select e.FirstName, e.LastName
from employees e
where LENGTH(e.LastName) = 5;
# Write a SQL query to display the current date and time in the following format "`day.month.year hour:minutes:seconds:milliseconds`".
select date_format(current_timestamp(2), '%d.%m.%Y.%h:%i:%s:%f');
# Write a SQL query to display the average employee salary by department and job title.
select AVG(e.Salary), d.Name
from employees e
         join departments d on e.DepartmentID = d.DepartmentID
group by d.DepartmentID
union
select AVG(e2.Salary), e2.JobTitle
from employees e2
group by e2.JobTitle;
# Write a SQL query to display the town where maximal number of employees work.
select count(e.EmployeeID), t.Name
from employees e
         join addresses a on e.AddressID = a.AddressID
         join towns t on a.TownID = t.TownID
group by t.TownID
order by count(e.EmployeeID) desc
limit 1;
# Write a SQL query to display the number of managers from each town.
select m.FirstName, m.LastName, t.Name, count(t.TownID)
from employees as m
         join addresses a on m.AddressID = a.AddressID
         join towns t on a.TownID = t.TownID
where EmployeeID in (
    select ManagerID
    from employees
    group by ManagerID)
group by t.TownID;
# Write a SQL query to find the manager who is in charge of the most employees and their average salary.

select m.FirstName, m.LastName, AVG(e.Salary), count(e.EmployeeID)
from employees e
         join employees m on e.ManagerID = m.EmployeeID
group by e.ManagerID
order by count(e.EmployeeID) desc
limit 1;



# Create a table `Users`. Users should have username, password, full name and last login time.
# Choose appropriate data types for the table fields. Define a primary key column with a primary key constraint.
#   - Define the primary key column as auto-increment to facilitate inserting records.
#   - Define unique constraint to avoid repeating usernames.
#   - Define a

#check constraint to ensure the password is at least 5 characters long.

CREATE TABLE users
(
    id         int auto_increment unique key primary key,
    username   VARCHAR(50) unique,
    password   VARCHAR(50),
    name       nvarchar(50),
    last_login datetime
);

# Write SQL statements to insert in the `Users` table the names of all employees from the `Employees` table.
#    - Combine the first and last names as a full name.
#    - For username
#use the first 3 letters of the first name + the last name (in lowercase).
#Use the same for the password.
insert into users(name)
select concat(e.FirstName, ' ', e.LastName)
from employees e;

update users
set username = (select concat(lower(left(e.FirstName, 3)), lower(left(e.LastName, 3)), e.EmployeeID)
                from employees e
                where id = e.EmployeeID);

update users
set password = (select concat(lower(right(e.FirstName, 3)), lower(right(e.LastName, 3)), e.EmployeeID)
                from employees e
                where id = e.EmployeeID);

update users
set last_login = (select cast(HireDate as datetime)
                  from employees
                  where id = EmployeeID);
#Use HireDate for last login time.
# Write a SQL statement that changes the password to `NULL` for all users that have not been in the system since year 1999.
update users
set password = null
where year(last_login) < 1999;
# Write a SQL statement that deletes all users without passwords (`NULL` password).
delete
from users
where password is null;
# Write a SQL statement finds the names of the employees who have worked on the most projects.
select count(e.ProjectID), group_concat(e.ProjectID)
from employees
         join employeesprojects e on employees.EmployeeID = e.EmployeeID
group by e.EmployeeID;

