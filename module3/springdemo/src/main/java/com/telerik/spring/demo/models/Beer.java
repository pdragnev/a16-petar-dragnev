package com.telerik.spring.demo.models;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class Beer {
    @PositiveOrZero(message = "ID should be 0 or greater")
    private int id;
    @Size(min = 2, max = 25, message = "Name size should be between 2 and 25 symbols")
    private String name;
    @PositiveOrZero(message = "ID should be 0 or greater")
    private double abv;

    public Beer(int id, String name, double abv) {
        this.id = id;
        this.name = name;
        this.abv = abv;
    }

    public Beer() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    @Override
    public String toString() {
        return String.format("\n" +
                "    \"id\": %d,\n" +
                "    \"name\": %s,\n" +
                "    \"abv\": %.2f\n",getId(),getName(),getAbv());
    }
}
