package com.telerik.spring.demo.controller;

import com.telerik.spring.demo.models.Beer;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/beers")
public class BeersController {
    private List<Beer> beers;

    public BeersController() {
        beers = new ArrayList<>();
        beers.add(new Beer(1, "Heineken", 2.2));
        beers.add(new Beer(2, "Pirinsko", 2.2));
        beers.add(new Beer(3, "Kameniza", 5.2));
    }


 /*   @GetMapping
    private Beer printBeerByName(@RequestParam(required = false) String name){
        return beers.stream()
                .filter(b -> b.getName().equals(name))
                .findAny()
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("We are out of %s", name)));
    }
*/
    @GetMapping
    public String getBeers(@RequestParam(required = false) String name) {
        if (name == null) {
            return beers.toString();
        }
        Beer beer = beers.stream()
                .filter(b -> b.getName().equals(name))
                .findAny()
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("We are out of %s", name)));
        return beer.toString();



    }

    @GetMapping("/{id}")
    public Beer getById(@PathVariable int id) {
        return getBeerById(id);
    }

    @PostMapping
    public void create(@RequestBody @Valid Beer newBeer) {
        beers.add(newBeer);
    }

    @PutMapping
    public Beer update(@RequestBody @Valid Beer beer) {
        Beer beerToUpdate = getBeerById(beer.getId());
        beerToUpdate.setName(beer.getName());
        beerToUpdate.setAbv(beer.getAbv());

        return beerToUpdate;
    }

    @DeleteMapping("/{id}")
    public Beer delete(@PathVariable int id){
        Beer beerToDelete = getBeerById(id);
        beers.remove(beerToDelete);
        return beerToDelete;
    }

    private Beer getBeerById(@PathVariable int id) {
        return beers.stream()
                .filter(b -> b.getId() == id)
                .findAny()
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("We are out of %d", id)));
    }
}

