package controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/aaaaa")
public class HelloWorldController {

    @GetMapping
    public String sayHi(){
        return "Hello API";
    }
}
